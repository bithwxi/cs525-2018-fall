/*
// A basic runtime for lambda
*/

/* ****** ****** */

#include <stdio.h>
#include <stdlib.h>

/* ****** ****** */

#define TAGint 1
#define TAGbool 2
#define TAGstring 3
#define TAGfunclo 4
#define TAGlist 5
#define TAGarray 6

/* ****** ****** */

typedef
struct{ int tag; } lamval_;

/* ****** ****** */

typedef
struct{ int tag; int data; } lamval_int_;
typedef
struct{ int tag; int data; } lamval_bool_;
typedef
struct{ int tag; char *data; } lamval_string_;

typedef
struct{ int tag; void **fenv; } lamval_funclo_;

/* ****** ****** */

typedef lamval_ *lamval;
typedef lamval_int_ *lamval_int;
typedef lamval_bool_ *lamval_bool;
typedef lamval_string_ *lamval_string;
typedef lamval_funclo_ *lamval_funclo;

/* ****** ****** */

/*
typedef
struct{ int tag; lamval head; lamval tail; } lamval_list_;

typedef
struct{ int tag; size_t size; lamval data[]; } lamval_array_;
*/

/* ****** ****** */

#define \
FUNCLO_ENV(cfn) \
(((lamval_funclo)cfn)->fenv)
#define \
FUNCLO_FUN(cfn) \
((lamval(*)(lamval, void**))(((lamval_funclo)cfn)->fenv[0]))

#define \
FUNCLO_APP(cfn, arg) \
FUNCLO_FUN(cfn)(arg, FUNCLO_ENV(cfn))

/* ****** ****** */

#if(0)
#define		 \
FUNCLO_FUN2(cfn) \
((lamval(*)(lamval, lamval, void**))(((lamval_funclo)cfn)->fenv[0]))
#define \
FUNCLO_APP2(cfn, arg1, arg2) \
FUNCLO_FUN2(cfn)(arg1, arg2, FUNCLO_ENV(cfn))
#endif

/* ****** ****** */

lamval
T3Vint(int i)
{
  lamval_int p0;
  p0 = malloc(sizeof(lamval_int_));
  p0->tag = TAGint; p0->data = i; return (lamval)p0;
}

lamval
T3Vbool(int i)
{
  lamval_bool p0;
  p0 = malloc(sizeof(lamval_bool_));
  p0->tag = TAGbool; p0->data = i; return (lamval)p0;
}

lamval
T3Vstring(char *cs)
{
  lamval_string p0;
  p0 = malloc(sizeof(lamval_string_));
  p0->tag = TAGstring; p0->data = cs; return (lamval)p0;
}

/* ****** ****** */

#define T3Vreg(reg) reg
#define T3Varg(arg) arg

lamval
T3Vfix(lamval *env)
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo; p0->fenv = (void**)env; return (lamval)p0;
}

/* ****** ****** */

#define T3Iift(x) if(((lamval_bool)x)->data)

/* ****** ****** */

lamval
T3Vopr_add(lamval x, lamval y)
{
  return
  T3Vint(((lamval_int)x)->data + ((lamval_int)y)->data);
}

lamval
T3Vopr_sub(lamval x, lamval y)
{
  return
  T3Vint(((lamval_int)x)->data - ((lamval_int)y)->data);
}

lamval
T3Vopr_mul(lamval x, lamval y)
{
  return
  T3Vint(((lamval_int)x)->data * ((lamval_int)y)->data);
}

lamval
T3Vopr_div(lamval x, lamval y)
{
  return
  T3Vint(((lamval_int)x)->data / ((lamval_int)y)->data);
}

lamval
T3Vopr_lt(lamval x, lamval y)
{
  return
  T3Vbool(((lamval_int)x)->data < ((lamval_int)y)->data);
}
lamval
T3Vopr_gt(lamval x, lamval y)
{
  return
  T3Vbool(((lamval_int)x)->data > ((lamval_int)y)->data);
}
lamval
T3Vopr_lte(lamval x, lamval y)
{
  return
  T3Vbool(((lamval_int)x)->data <= ((lamval_int)y)->data);
}
lamval
T3Vopr_gte(lamval x, lamval y)
{
  return
  T3Vbool(((lamval_int)x)->data >= ((lamval_int)y)->data);
}

/* ****** ****** */

/* end of [runtime.h] */
