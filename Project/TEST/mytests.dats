(* ****** ****** *)

val
T1Madd =
(
T1Mlam("x", T1Mlam("y", T1Madd(x, y)))
) where
{
  val x = T1Mvar("x") and y = T1Mvar("y")
}

val
T2Madd = trans12_term(T1Madd)

(* ****** ****** *)

val
T1Mfact =
T1Mfix
( "f", "x"
, T1Mift
  ( T1Mgte(x, T1Mint(1))
  , T1Mmul(x, T1Mapp(f, T1Mpred(x))), T1Mint(1))
) where
{
  val x = T1Mvar("x") and f = T1Mvar("f")
}

val
T1Mfact_ann =
T1Mann(T1Mfact, T1Pfun(T1Pint, T1Pint))

val T2Mfact = trans12_term(T1Mfact_ann)

val T1Pfact = tinfer0(T2Mfact)

val ((*void*)) =
println!("T1Pfact = ", T1Pfact)

val ((*void*)) =
println!("fact(10) = ", interp0(T2Mapp(T2Mfact, T2Mint(10))))

(* ****** ****** *)

val
T1Mfact1 =
(
T1Mlam
( "n"
,
T1Mapp2
(T1Mfix("loop", "i", T1Mlam("r", T1Mift(T1Mlt(i, n), T1Mapp2(loop, T1Msucc(i), T1Mmul(T1Msucc(i), r)), r))), T1Mint(0), T1Mint(1))
)
) where
{
  val n = T1Mvar("n")
  val i = T1Mvar("i")
  val r = T1Mvar("r")
  val loop = T1Mvar("loop")
}

val T2Mfact1 =
trans12_term(T1Mfact1)

val T1Pfact1 = tinfer0(T2Mfact1)

val ((*void*)) =
println!("T1Pfact1 = ", T1Pfact1)

val ((*void*)) =
println!("fact1(10) = ", interp0(T2Mapp(T2Mfact1, T2Mint(10))))

(* ****** ****** *)

val
T1Mfact2 =
(
T1Mlam
("n",
T1Mapp3
( T1Mfoldleft_int()
, n, T1Mint(1)
, T1Mlam("r", T1Mlam("i", T1Mmul(r, T1Msucc(i)))))
)
) where
{
  val n = T1Mvar("n") and r = T1Mvar("r") and i = T1Mvar("i")
}

val
T2Mfact2 = trans12_term(T1Mfact2)

val T1Pfact2 = tinfer0(T2Mfact2)

val ((*void*)) =
println!("T1Pfact2 = ", T1Pfact2)

val ((*void*)) =
println!("fact2(10) = ", interp0(T2Mapp(T2Mfact2, T2Mint(10))))

(* ****** ****** *)

val
T1Marray_copy =
(
T1Mlam
(
"A1"
,
T1Mlet("sz", T1Marr_size(A1),
T1Mlet("a0", T1Marr_get_at(A1, T1Mint(0)),
T1Mlet("A2", T1Marr_new(sz, a0),
T1Mlet("loop",
       T1Mfix("f", "i",
              T1Mift(T1Mlt(i, sz), T1Mlet("", T1Marr_set_at(A2, i, T1Marr_get_at(A1, i)), T1Mapp(f, T1Msucc(i))), T1Munit)),
       T1Mapp(loop, T1Mint(1))
      )
      )
      )
      )

)
) where
{
  val A1 = T1Mvar("A1")
  val sz = T1Mvar("sz")
  val a0 = T1Mvar("a0")
  val A2 = T1Mvar("A2")
  val loop = T1Mvar("loop")
  val f = T1Mvar("f") and i = T1Mvar("i")
}

val
T1Parray_copy = tinfer0(trans12_term(T1Marray_copy))

(* ****** ****** *)

(* end of [mytests.dats] *)
