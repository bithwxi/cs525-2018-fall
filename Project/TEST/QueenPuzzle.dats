(*
fun
fsolve
(i: int): list0(list0(int)) =
if (i = 0) then sing(nil0) else fsolve(i-1)
*)
(* ****** ****** *)

#define N 8

(* ****** ****** *)

val
T1Mfsolve =
(
(
T1Mfix
( "f0"
, "i0"
, T1Mift
  ( T1Mgt(i0, T1Mint(0))
  , T1Mapp(T1Mextend_all, T1Mapp(f0, T1Mpred(i0)))
  , T1Mcons(T1Mnil(), T1Mnil()))
) where
{
  val f0 = T1Mvar("f0")
  val i0 = T1Mvar("i0")
}
) where
{
val
T1Mextend_all =
(
T1Mlam
( "xss"
, T1Mapp(T1Mconcat_list(), T1Mapp2(T1Mmap_list(), xss, T1Mextend_one)))
) where
{
  val xss = T1Mvar("xss")
}
}
) where
{
val
T1Mextend_one =
T1Mlam
(
"xs"
,
T1Mapp2
(
T1Mfilter_list()
,
T1Mapp
(
T1Mfix
( "f1", "i1"
, T1Mift(T1Mlt(i1, T1Mint(N)), T1Mcons(T1Mcons(i1, xs), T1Mapp(f1, T1Msucc(i1))), T1Mnil())
)
,
T1Mint(0)
)
,
T1Mlam
(
"x0xs",
T1Mlet
( "x0"
, T1Mhead(x0xs)
, T1Mapp2
  ( T1Miforall_list()
  , T1Mtail(x0xs)
  , T1Mlam("i1", T1Mlam("x1", T1Mand(T1Mneq(x0, x1), T1Mneq(T1Mapp(T1Mabs, T1Msub(x0, x1)), T1Msucc(i1))))))
)
)
)
) where
{
  val xs = T1Mvar("xs")
  val f1 = T1Mvar("f1")
  val i1 = T1Mvar("i1")
  val x0 = T1Mvar("x0")
  val x1 = T1Mvar("x1")
  val x0xs = T1Mvar("x0xs")
}
}

(* ****** ****** *)

val
T1Mqueenpuzzlesolve =
T1Mapp2
(
T1Mforeach_list(),
T1Mapp(T1Mfsolve, T1Mint(N)),
T1Mlam
(
"xs"
,
T1Mseq
(
T1Mapp2
( T1Mrforeach_list()
, xs
, T1Mlam
  ( "x"
  , T1Mapp
    (T1Mfix
     ( "p", "i"
     , T1Mift
       (T1Mlt(i, T1Mint(N)),
        T1Mseq(T1Mprstr(T1Mift(T1Meq(i, x), T1Mstring("Q "), T1Mstring(". "))), T1Mapp(p, T1Msucc(i))),
        T1Mprstr(T1Mstring("\n"))
       )
     ), T1Mint(0)
    )
  )
), T1Mprstr(T1Mstring("\n"))
)
)
) where
{
  val x = T1Mvar("x") and xs = T1Mvar("xs") and p = T1Mvar("p") and i = T1Mvar("i") 
}

(* ****** ****** *)

val
T2Mqueenpuzzlesolve =
trans12_term
(T1Mqueenpuzzlesolve)
val
T1Pqueenpuzzlesolve =
tinfer0(T2Mqueenpuzzlesolve)
val ((*void*)) =
println!
("T1Pqueenpuzzlesolve = ", T1Pqueenpuzzlesolve)
val ((*void*)) =
println!
("T2Mqueenpuzzlesolve = \n\n", interp0(T2Mqueenpuzzlesolve))

(* ****** ****** *)

(* end of [QueenPuzzle.dats] *)
