/* ****** ****** */
/*
// A basic runtime for lambda
*/
/* ****** ****** */

#include "runtime.h"

/* ****** ****** */

extern
lamval
F0(lamval arg, lamval *env);
extern
lamval
F1(lamval arg, lamval *env);

extern
lamval
F0_funclo();
extern
lamval
F1_funclo(lamval n);

/* ****** ****** */

lamval
F0(lamval arg0, lamval* env)
{

  return F1_funclo(arg0);
}

lamval
F1(lamval arg1, lamval* env)
{
  lamval R0;
  R0 = T3Vopr_add(arg1, env[1]);

  return R0;
}

lamval
F0_funclo()
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo;
  p0->fenv = malloc(1 * sizeof(void *));
  p0->fenv[0] = &F0;
  return (lamval)p0;
}

lamval
F1_funclo(lamval arg0)
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo;
  p0->fenv = malloc(2 * sizeof(void *));
  p0->fenv[0] = &F1; p0->fenv[1] = arg0;
  return (lamval)p0;
}



int
main()
{
  lamval Rf, Rr;
  Rf = F0_funclo();
  Rr = FUNCLO_APP(FUNCLO_APP(Rf, T3Vint(5)), T3Vint(10));
  printf("add2.c(5) = %i\n", ((lamval_int)Rr)->data);
  return 0;
}
