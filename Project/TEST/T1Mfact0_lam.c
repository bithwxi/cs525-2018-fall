/* ****** ****** */
/*
// A basic runtime for lambda
*/
/* ****** ****** */

#include "runtime.h"

/* ****** ****** */

extern
lamval
F0_funclo();

/*

fact(n) =
if n >= 1 then n * fact(n-1) else 1

*/

#if(0)
lamval
F0(lamval arg, lamval *env)
{
  lamval R0, R1, R2, R3, R4, Rf;
  R1 = T3Vopr_gte(arg, T3Vint(1));
  T3Iift(R1)
  {
    R4 = T3Vopr_sub(arg, T3Vint(1));
    /*
    Rf = F0_funclo();
    R3 = FUNCLO_APP(Rf, R4);
    */
    R3 = FUNCLO_APP(T3Vfix(env), R4);
    R2 = T3Vopr_mul(arg, R3);
    R0 = R2;
  } else {
    R0 = T3Vint(1);
  }
  return R0;
}
#endif

lamval
F0(lamval arg , lamval *env)
{

lamval R_11, R_12, R_13, R_14, R_15;
  
R_12 = T3Vopr_gt(arg, T3Vint(0));

T3Iift(T3Vreg(R_12))
{
R_15 = T3Vopr_sub(arg, T3Vint(1));
R_14 = FUNCLO_APP(T3Vfix(env), T3Vreg(R_15)) ;
R_13 = T3Vopr_mul(arg, T3Vreg(R_14));
R_11 = T3Vreg(R_13) ; 
} else {
R_11 = T3Vint(1) ; 
}

return R_11;

}


lamval
F0_funclo()
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo;
  p0->fenv = malloc(1*sizeof(void*));
  p0->fenv[0] = &F0; return (lamval)p0;
}

/* ****** ****** */

int
main()
{
  lamval Rf, Rr;
  Rf = F0_funclo();
  Rr = FUNCLO_APP(Rf, T3Vint(5));
  printf("fact(5) = %i\n", ((lamval_int)Rr)->data);
  return 0;
}

/* ****** ****** */

/* end of [T1Mfact0_lam.c] */
