(* ****** ****** *)

val T1Mtrue = T1Mbool(true)
val T1Mfalse = T1Mbool(false)

(* ****** ****** *)

fun
T1Mor
( x:t1erm
, y:t1erm): t1erm = T1Mift(x, T1Mtrue, y)
fun
T1Mand
( x:t1erm
, y:t1erm): t1erm = T1Mift(x, y, T1Mfalse)

(* ****** ****** *)

fun
T1Mseq
( x:t1erm
, y:t1erm): t1erm = T1Mlet("", x, y)


(* ****** ****** *)

macdef
T1Mprstr(str) = T1Mopr("prstr", ,(str) :: nil0())

(* ****** ****** *)

(*
macdef
T1Mapp2
(f, x, y) = T1Mapp(T1Mapp(,(f), ,(x)), ,(y))
macdef
T1Mapp3
(f, x, y, z) = T1Mapp(T1Mapp(T1Mapp(,(f), ,(x)), ,(y)), ,(z))
*)

fun
T1Mapp2
( f:t1erm
, x:t1erm
, y:t1erm): t1erm = T1Mapp(T1Mapp(f, x), y)
fun
T1Mapp3
( f:t1erm
, x:t1erm
, y:t1erm
, z:t1erm): t1erm = T1Mapp(T1Mapp(T1Mapp(f, x), y), z)

(* ****** ****** *)

macdef
T1Madd(x, y) = T1Mopr("+", ,(x) :: ,(y) :: nil0())
macdef
T1Msub(x, y) = T1Mopr("-", ,(x) :: ,(y) :: nil0())
macdef
T1Mmul(x, y) = T1Mopr("*", ,(x) :: ,(y) :: nil0())

(* ****** ****** *)

macdef
T1Mneg(x) = T1Msub(T1Mint(0), ,(x))

(* ****** ****** *)

macdef
T1Msucc(x) = T1Madd(,(x), T1Mint(1))
macdef
T1Mpred(x) = T1Msub(,(x), T1Mint(1))

(* ****** ****** *)

macdef
T1Meq(x, y) = T1Mopr("=", ,(x) :: ,(y) :: nil0())
macdef
T1Mneq(x, y) = T1Mopr("!=", ,(x) :: ,(y) :: nil0())

(* ****** ****** *)

macdef
T1Mlt(x, y) = T1Mopr("<", ,(x) :: ,(y) :: nil0())
macdef
T1Mgt(x, y) = T1Mopr(">", ,(x) :: ,(y) :: nil0())
macdef
T1Mlte(x, y) = T1Mopr("<=", ,(x) :: ,(y) :: nil0())
macdef
T1Mgte(x, y) = T1Mopr(">=", ,(x) :: ,(y) :: nil0())

(* ****** ****** *)

val
T1Mabs =
(
T1Mlam
("x", T1Mift(T1Mgte(x, T1Mint(0)), x, T1Mneg(x)))
) where
{
  val x = T1Mvar("x")
}

(* ****** ****** *)

(*
forall_int
(n: int, f: (int) -> bool) -> bool
*)
val
T1Mforall_int: t1erm =
(
T1Mlam("n", T1Pint,
T1Mlam("f", T1Pfun(T1Pint, T1Pbool),
T1Mapp
(
T1Mfix("loop", "i", T1Pint, T1Pbool,
       T1Mift(T1Mlt(i, n), T1Mift(T1Mapp(f, i), T1Mapp(loop, T1Msucc(i)), T1Mbool(false)), T1Mbool(true))
      )
,
T1Mint(0)
)
)
)
) where
{
  val f = T1Mvar("f")
  val n = T1Mvar("n")
  val i = T1Mvar("i")
  val loop = T1Mvar("loop")  
}

val
T2Mforall_int
: t2erm = trans12_term(T1Mforall_int)
val
T1Pforall_int: t1ype = tinfer0(T2Mforall_int)

(* ****** ****** *)

(*
foreach_int
(n: int, f: (int) -> void) -> void
*)
val
T1Mforeach_int: t1erm =
(
T1Mlam
("n",
T1Mlam
("f",
T1Mlet
("f1",
T1Mlam
("i", T1Mseq(T1Mapp(f, i), T1Mbool(true))),
T1Mseq
(T1Mapp2(T1Mforall_int, n, f1), T1Munit())
)
)
)
) where
{
  val f = T1Mvar("f")
  val n = T1Mvar("n")
  val i = T1Mvar("i")
  val f1 = T1Mvar("f1")
}

val
T2Mforeach_int
: t2erm = trans12_term(T1Mforeach_int)
val
T1Pforeach_int: t1ype = tinfer0(T2Mforeach_int)

(* ****** ****** *)

(*
foldleft_int
(n: int, res, f: (res, int) -> res) -> bool
*)
fun
T1Mfoldleft_int(): t1erm =
(
T1Mlam("n",
T1Mlam("r",
T1Mlam("f",
T1Mapp2
(
T1Mfix("loop", "i", T1Mlam("r", T1Mift(T1Mlt(i, n), T1Mapp2(loop, T1Msucc(i), T1Mapp2(f, r, i)), r)))
,
T1Mint(0), r
)
)
)
)
) where
{
  val f = T1Mvar("f")
  val n = T1Mvar("n")
  val r = T1Mvar("r")
  val i = T1Mvar("i")
  val loop = T1Mvar("loop")  
}
//
fun
T2Mfoldleft_int()
: t2erm = trans12_term(T1Mfoldleft_int())
val
T1Pfoldleft_int: t1ype = tinfer0(T2Mfoldleft_int())
//
val () =
println! ("T1Pfoldleft_int = ", T1Pfoldleft_int)
//
(* ****** ****** *)
//
(*
forall_list
(n: list, f: (elt) -> bool) -> bool
*)
//
fun
T1Mforall_list(): t1erm =
(
T1Mlam("xs",
T1Mlam("f0",
T1Mapp
(
T1Mfix
(
"loop", "xs",
T1Mift(T1Misnil(xs), T1Mtrue, T1Mift(T1Mapp(f0, T1Mhead(xs)), T1Mapp(loop, T1Mtail(xs)), T1Mfalse))
)
,
xs
)
)
)
) where
{
  val f0 = T1Mvar("f0")
  val xs = T1Mvar("xs")
  val loop = T1Mvar("loop")  
}

fun
T2Mforall_list()
: t2erm = trans12_term(T1Mforall_list())
val
T1Pforall_list: t1ype = tinfer0(T2Mforall_list())
//
val () =
println!("T1Pforall_list = ", T1Pforall_list)
//
(* ****** ****** *)
//
(*
rforall_list
(n: list, f: (elt) -> bool) -> bool
*)
//
fun
T1Mrforall_list(): t1erm =
(
T1Mlam("xs",
T1Mlam("f0",
T1Mapp
(
T1Mfix
(
"auxlst", "xs",
T1Mift(T1Misnil(xs), T1Mtrue, T1Mift(T1Mapp(auxlst, T1Mtail(xs)), T1Mapp(f0, T1Mhead(xs)), T1Mfalse))
)
,
xs
)
)
)
) where
{
  val f0 = T1Mvar("f0")
  val xs = T1Mvar("xs")
  val auxlst = T1Mvar("auxlst")  
}

fun
T2Mrforall_list()
: t2erm = trans12_term(T1Mrforall_list())
val
T1Prforall_list: t1ype = tinfer0(T2Mrforall_list())
//
val () =
println!("T1Prforall_list = ", T1Prforall_list)
//
(* ****** ****** *)

(*
foreach_list
(n: int, f: (int) -> void) -> void
*)
fun
T1Mforeach_list(): t1erm =
(
T1Mlam
("xs",
T1Mlam
("f0",
T1Mlet
("f1",
T1Mlam
("x0", T1Mseq(T1Mapp(f0, x0), T1Mbool(true))),
T1Mseq
(T1Mapp2(T1Mforall_list(), xs, f1), T1Munit())
)
)
)
) where
{
  val xs = T1Mvar("xs")
  val f0 = T1Mvar("f0")
  val f1 = T1Mvar("f1")
  val x0 = T1Mvar("x0")
}

fun
T2Mforeach_list()
: t2erm = trans12_term(T1Mforeach_list())
val
T1Pforeach_list: t1ype = tinfer0(T2Mforeach_list())
//
val () =
println!("T1Pforeach_list = ", T1Pforeach_list)
//
(* ****** ****** *)

(*
rforeach_list
(n: int, f: (int) -> void) -> void
*)
fun
T1Mrforeach_list(): t1erm =
(
T1Mlam
("xs",
T1Mlam
("f0",
T1Mlet
("f1",
T1Mlam
("x0", T1Mseq(T1Mapp(f0, x0), T1Mbool(true))),
T1Mseq
(T1Mapp2(T1Mrforall_list(), xs, f1), T1Munit())
)
)
)
) where
{
  val xs = T1Mvar("xs")
  val f0 = T1Mvar("f0")
  val f1 = T1Mvar("f1")
  val x0 = T1Mvar("x0")
}

fun
T2Mrforeach_list()
: t2erm = trans12_term(T1Mrforeach_list())
val
T1Prforeach_list: t1ype = tinfer0(T2Mrforeach_list())
//
val () =
println!("T1Prforeach_list = ", T1Prforeach_list)
//
(* ****** ****** *)
//
(*
forall_list
(n: list, f: (elt) -> bool) -> bool
*)
//
fun
T1Miforall_list(): t1erm =
(
T1Mlam("xs",
T1Mlam("f0",
T1Mapp2
(
T1Mfix
(
"loop", "i0",
T1Mlam
(
"xs",
T1Mift
( T1Misnil(xs)
, T1Mtrue
, T1Mift(T1Mapp2(f0, i0, T1Mhead(xs)), T1Mapp2(loop, T1Msucc(i0), T1Mtail(xs)), T1Mfalse))
)
)
,
T1Mint(0), xs
)
)
)
) where
{
  val f0 = T1Mvar("f0")
  val i0 = T1Mvar("i0")
  val xs = T1Mvar("xs")
  val loop = T1Mvar("loop")  
}

fun
T2Miforall_list()
: t2erm = trans12_term(T1Miforall_list())
val
T1Piforall_list: t1ype = tinfer0(T2Miforall_list())
//
val () =
println!("T1Piforall_list = ", T1Piforall_list)
//
(* ****** ****** *)

fun
T1Mmap_list(): t1erm =
(
T1Mfix
( "f0"
, "xs"
, T1Mlam
  ( "fopr"
  , T1Mift
    ( T1Misnil(xs)
    , T1Mnil(*void*)
    , T1Mcons(T1Mapp(fopr, T1Mhead(xs)), T1Mapp2(f0, T1Mtail(xs), fopr)))
  )
)
) where
{
  val f0 = T1Mvar("f0")
  val xs = T1Mvar("xs")
  val fopr = T1Mvar("fopr")
}

fun
T2Mmap_list()
: t2erm = trans12_term(T1Mmap_list())
val
T1Pmap_list: t1ype = tinfer0(T2Mmap_list())
//
val () =
println!("T1Pmap_list = ", T1Pmap_list)
//
(* ****** ****** *)

fun
T1Mfilter_list(): t1erm =
(
T1Mfix
( "f0"
, "xs"
, T1Mlam
  ( "fopr"
  , T1Mift
    ( T1Misnil(xs)
    , T1Mnil(*void*)
    , T1Mlet
      ( "x0"
      , T1Mhead(xs)
      , T1Mift
        ( T1Mapp(fopr, x0)
        , T1Mcons(x0, T1Mapp2(f0, T1Mtail(xs), fopr))
        , T1Mapp2(f0, T1Mtail(xs), fopr)
        )
      )
    )
  )
)
) where
{
  val f0 = T1Mvar("f0")
  val x0 = T1Mvar("x0")
  val xs = T1Mvar("xs")
  val fopr = T1Mvar("fopr")
}

fun
T2Mfilter_list()
: t2erm = trans12_term(T1Mfilter_list())
val
T1Pfilter_list: t1ype = tinfer0(T2Mfilter_list())
//
val () =
println!("T1Pfilter_list = ", T1Pfilter_list)
//
(* ****** ****** *)

fun
T1Mappend_list(): t1erm =
(
T1Mfix
( "f0"
, "xs"
, T1Mlam("ys", T1Mift(T1Misnil(xs), ys, T1Mcons(T1Mhead(xs), T1Mapp2(f0, T1Mtail(xs), ys))))
)
) where
{
  val f0 = T1Mvar("f0")
  val xs = T1Mvar("xs")
  val ys = T1Mvar("ys")
}

fun
T2Mappend_list()
: t2erm = trans12_term(T1Mappend_list())
val
T1Pappend_list: t1ype = tinfer0(T2Mappend_list())
//
val () =
println!("T1Pappend_list = ", T1Pappend_list)
//
(* ****** ****** *)
//
fun
T1Mconcat_list(): t1erm =
(
T1Mlet
(
"append"
,
T1Mappend_list()
,
T1Mfix
( "f0"
, "xss"
, T1Mift(T1Misnil(xss), T1Mnil(), T1Mapp2(append, T1Mhead(xss), T1Mapp(f0, T1Mtail(xss))))
)
)
) where
{
  val f0 = T1Mvar("f0")
  val xss = T1Mvar("xss")
  val append = T1Mvar("append")
}

fun
T2Mconcat_list()
: t2erm = trans12_term(T1Mconcat_list())
val
T1Pconcat_list: t1ype = tinfer0(T2Mconcat_list())
//
val () =
println!("T1Pconcat_list = ", T1Pconcat_list)
//
(* ****** ****** *)

(* end of [mylibs.dats] *)
