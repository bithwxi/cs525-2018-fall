(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#staload "./syntax1.dats"
//
(* ****** ****** *)

typedef
t1xyz = $rec
{ stamp= int
, t1ype= ref(t1ypeopt)
} (* end of [t1xyz] *)

(* ****** ****** *)

assume t1xyz_type = t1xyz

(* ****** ****** *)

local

val
theStamp = ref<int>(0)
fun
theStamp_getinc() =
let
val n = !theStamp in !theStamp := n+1; n
end // end of [theStamp_getinc]

in (* in-of-local *)

implement
t1xyz_new() = let
  val stamp = theStamp_getinc()
in
  $rec{ stamp= stamp, t1ype=ref(None0()) }
end // end of [t1xyz_new]

end // end of [local]

(* ****** ****** *)
//
implement
eq_t1xyz_t1xyz
  (X1, X2) =
  (X1.stamp = X2.stamp)
//
(* ****** ****** *)
//
implement
t1xyz_get_t1ype
  (X) = !(X.t1ype)
implement
t1xyz_set_t1ype
  (X, T) = !(X.t1ype) := Some0(T)
//
(* ****** ****** *)

implement
print_t1xyz(X) =
fprint_t1xyz(stdout_ref, X)
implement
prerr_t1xyz(X) =
fprint_t1xyz(stderr_ref, X)

implement
fprint_t1xyz
  (out, X) = let
//
  val S = X.stamp
  val opt = !(X.t1ype)
//
in
case+ opt of
| None0() =>
  (
    fprint!(out, "X(", S, ")")
  )
| Some0(T) =>
  (
    fprint!(out, "X(", S, ")[", T, "]")
  )
end // end of [fprint_t1xyz]

(* ****** ****** *)

(* end of [syntax1_txyz.dats] *)
