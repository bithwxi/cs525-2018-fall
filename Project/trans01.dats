(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#staload "./syntax0.dats"
#staload "./syntax1.dats"
//
(* ****** ****** *)
//
extern
fun
trans01_type : type -> t1ype
//
extern
fun
trans01_term : term -> t1erm
extern
fun
trans01_termlst : termlst -> t1ermlst
//
(* ****** ****** *)

overload trans01 with trans01_type
overload trans01 with trans01_term

(* ****** ****** *)

implement
trans01_type
  (T0) =
(
case+ T0 of
| TPbas(nm) => T1Pbas(nm)
| TPfun(T1, T2) => T1Pfun(trans01(T1), trans01(T2))
| TPtup(T1, T2) => T1Ptup(trans01(T1), trans01(T2))
)

(* ****** ****** *)

local

fun
aux
(t0: term): t1erm =
(
case+ t0 of
| TMint(i0) => T1Mint(i0)
| TMvar(x0) => T1Mvar(x0)
//
| TMlam(ta, tb) =>
  (
  case+ ta of
  | TAvar(x0) =>
    T1Mlam(x0, trans01(tb))
  | TAann(x0, opt) =>
    (
      case+ opt of
      | None0() => T1Mlam(x0, trans01(tb))
      | Some0(T1) => T1Mlam(x0, trans01(T1), trans01(tb))
    )
  )
//
| TMapp(t1, t2) =>
  T1Mapp(trans01(t1), trans01(t2))
//
| TMopr(opr, ts) =>
  T1Mopr(opr, trans01_termlst(ts))
//
| TMlet(x0, t1, t2) =>
  T1Mlet(x0, trans01(t1), trans01(t2))
//
| TMfix(f0, ta, tb) =>
  (
   case+ ta of
   | TAvar(x0) =>
     T1Mfix(f0, x0, trans01(tb))
   | TAann(x0, opt) =>
     (
       case+ opt of
       | None0() =>
         T1Mfix(f0, x0, trans01(tb))
       | Some0(T1) =>
         let
           val T2 = T1Pvar(t1xyz_new())
         in
           T1Mfix(f0, x0, trans01(T1), T2, trans01(tb))
         end
     ) (* end of [TAann] *)
  )
//
| TMift(t1, t2, t3) =>
  T1Mift(trans01(t1), trans01(t2), trans01(t3))
| TMann(t1, T2) => T1Mann(trans01(t1), trans01(T2))
)

in (* in-of-local *)

implement
trans01_term(t0) = aux(t0)
implement
trans01_termlst(ts) =
list0_map<term><t1erm>(ts, lam(t) => aux(t))

end // end of [local]

(* ****** ****** *)

(* end of [trans01.dats] *)
