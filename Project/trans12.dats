(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#staload "./syntax1.dats"
#staload "./syntax2.dats"
//
(* ****** ****** *)
//
extern
fun
trans12_term : t1erm -> t2erm
extern
fun
trans12_termlst : t1ermlst -> t2ermlst
//
(* ****** ****** *)

overload trans12 with trans12_term

(* ****** ****** *)

local

typedef
env_t =
list0($tup(t1var, t2var))

fun
aux
(t1m0: t1erm, env0: env_t): t2erm =
let
//
(*
val () =
println!("trans12: aux: t1m0 = ", t1m0)
*)
//
in
(
case+ t1m0 of
| T1Mint(i0) => T2Mint(i0)
//
| T1Mbool(b0) => T2Mbool(b0)
//
| T1Munit((*void*)) => T2Munit()
//
(*
| T1Mfloat of float // value
*)
| T1Mstring(s0) => T2Mstring(s0)
//
| T1Mvar(t1v) =>
  T2Mvar
  (
    aux_var(t1v, env0)
  ) where
  {
    fun
    aux_var
    ( t1v: t1var
    , env: env_t): t2var = let
      val opt2 =
      list0_find_opt
      (env, lam(kx) => (kx.0 = t1v))
      val-~Some_vt(kx) = opt2
    in
      kx.1
    end // end of [aux_var]
  }
| T1Mlam
  (t1v1, t1m3) => let
    val t2v1 = t2var_new(V2Karg)
    val t2m3 =
    aux(t1m3, cons0($tup(t1v1, t2v1), env0))
  in
    T2Mlam(t2v1, None0(), t2m3)
  end
| T1Mlam
  (t1v1, T2, t1m3) => let
    val t2v1 = t2var_new(V2Karg)
    val t2m3 =
    aux(t1m3, cons0($tup(t1v1, t2v1), env0))
  in
    T2Mlam(t2v1, Some0(T2), t2m3)
  end
//
| T1Mapp(t1m1, t1m2) =>
  (
    T2Mapp(aux(t1m1, env0), aux(t1m2, env0))
  )
//
| T1Mlet
  (t1v1, t1m2, t1m3) => let
    val t2m2 = aux(t1m2, env0)
    val t3r1 = t3reg_new()
    val t2v1 = t2var_new(V2Klet(t3r1))
    val env1 = cons0($tup(t1v1, t2v1), env0)
    val t2m3 = aux(t1m3, env1)
  in
    T2Mlet(t2v1, t2m2, t2m3)
  end
//
| T1Mopr(topr, t1ms) =>
  (
    T2Mopr(topr, auxlst(t1ms, env0))
  )
//
| T1Mfix
  (t1v1, t1v2, t1m3) => let
    val opt1 = None0()
    val opt2 = None0()
    val t2v1 = t2var_new(V2Kfix)
    val t2v2 = t2var_new(V2Karg)
    val env1 = cons0($tup(t1v1, t2v1), env0)
    val env2 = cons0($tup(t1v2, t2v2), env1)
  in
    T2Mfix(t2v1, t2v2, opt1, opt2, aux(t1m3, env2))
  end
| T1Mfix
  (t1v1, t1v2, T1, T2, t1m3) => let
    val opt1 = Some0(T1)
    val opt2 = Some0(T2)
    val t2v1 = t2var_new(V2Kfix)
    val t2v2 = t2var_new(V2Karg)
    val env1 = cons0($tup(t1v1, t2v1), env0)
    val env2 = cons0($tup(t1v2, t2v2), env1)
  in
    T2Mfix(t2v1, t2v2, opt1, opt2, aux(t1m3, env2))
  end
//
| T1Mtup(t1m1, t1m2) =>
  (
    T2Mtup(aux(t1m1, env0), aux(t1m2, env0))
  )
//
| T1Mfst(t1m1) => T2Mfst(aux(t1m1, env0))
| T1Msnd(t1m1) => T2Msnd(aux(t1m1, env0))
//
| T1Mift(t1m1, t1m2, t1m3) =>
  T2Mift
  (aux(t1m1, env0), aux(t1m2, env0), aux(t1m3, env0))
//
| T1Mann(t1m1, t1p2) => T2Mann(aux(t1m1, env0), t1p2)
//
| T1Mnil() => T2Mnil()
| T1Mcons(t1m1, t1m2) =>
  T2Mcons(aux(t1m1, env0), aux(t1m2, env0))
//
| T1Mhead(t1m1) =>
  T2Mhead(aux(t1m1, env0))
| T1Mtail(t1m1) =>
  T2Mtail(aux(t1m1, env0))
| T1Misnil(t1m1) =>
  T2Misnil(aux(t1m1, env0))
//
| T1Mref_new(t1m1) =>
  T2Mref_new(aux(t1m1, env0))
| T1Mref_get(t1m1) =>
  T2Mref_get(aux(t1m1, env0))
| T1Mref_set(t1m1, t1m2) =>
  T2Mref_set(aux(t1m1, env0), aux(t1m2, env0))
//
| T1Marr_new(t1m1, t1m2) =>
  T2Marr_new(aux(t1m1, env0), aux(t1m2, env0))
//
| T1Marr_size(t1m1) =>
  T2Marr_size(aux(t1m1, env0))
//
| T1Marr_get_at(t1m1, t1m2) =>
  T2Marr_get_at(aux(t1m1, env0), aux(t1m2, env0))
| T1Marr_set_at(t1m1, t1m2, t1m3) =>
  T2Marr_set_at(aux(t1m1, env0), aux(t1m2, env0), aux(t1m3, env0))
//
)
//
end // end of [let] // end of [aux]

and
auxlst
(t1ms: t1ermlst, env0: env_t): t2ermlst =
list0_map(t1ms, lam(t1m) => aux(t1m, env0))

in (* in-of-local *)

implement
trans12_term(t1m0) = aux(t1m0, list0_nil)

end // end of [local]

(* ****** ****** *)

(* end of [trans12.dats] *)
