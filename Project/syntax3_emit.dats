(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#staload "./syntax1.dats"
#staload "./syntax2.dats"
#staload "./syntax3.dats"
//
(* ****** ****** *)

extern
fun
temit3_int
(FILEref, int): void
extern
fun
temit3_str
(FILEref, string): void
extern
fun
temit3_eol(FILEref): void

(* ****** ****** *)
//
extern
fun
temit3_fun
(FILEref, t3fun): void
//
extern
fun
temit3_reg
(FILEref, t3reg): void
//
extern
fun
temit3_val
(FILEref, t3val): void

(* ****** ****** *)
//
extern
fun
temit3_T3Vfun
(out: FILEref, t3v: t3val): void
extern
fun
temit3_T3Vfun_dec
(out: FILEref, t3v: t3val): void
//
(* ****** ****** *)
//
extern
fun
temit3_var
(FILEref, t2var): void
//
(* ****** ****** *)

extern
fun
temit3_ins
(FILEref, t3ins): void
extern
fun
temit3_inss
(FILEref, t3inslst): void

(* ****** ****** *)

overload temit3 with temit3_int

overload temit3 with temit3_str

overload temit3 with temit3_reg

overload temit3 with temit3_val
overload temit3 with temit3_ins
overload temit3 with temit3_inss

(* ****** ****** *)
//
implement
temit3_int
(out, int) = fprint!(out, int)
implement
temit3_str
(out, txt) = fprint!(out, txt)
//
implement
temit3_eol(out) = fprint!(out, "\n")
//
(* ****** ****** *)

implement
temit3_fun
(out, f0) =
(
fprint(out, "F_");
fprint(out, f0.stamp())
)

(* ****** ****** *)

implement
temit3_reg
(out, r0) =
(
fprint(out, "R_");
fprint(out, r0.stamp())
)

(* ****** ****** *)

implement
temit3_var
  (out, x0) = let
  val knd = x0.v2knd()
in
  case+ knd of
  | V2Karg() => temit3_str(out, "arg")
  | V2Kfix() => temit3_str(out, "env")
  | V2Klet(t3r) => temit3_reg(out, t3r)
end

(* ****** ****** *)

implement
temit3_val
(out, t3v) =
(
case+ t3v of
| T3Vint(i0) =>
  fprint!(out, "T3Vint(", i0, ")")
| T3Vbool(b0) =>
  fprint!(out, "T3Vbool(", b0, ")")
| T3Vunit((*void*)) =>
  fprint!(out, "T3Vunit(", ")")
| T3Vstring(s0) =>
  fprint!(out, "T3Vstring(\"", s0, "\")")
//
| T3Vreg(r0) =>
  (
    temit3_str(out, "T3Vreg");
    temit3_str(out, "("); temit3_reg(out, r0); temit3_str(out, ")")
  )
| T3Vvar(t2v) =>
  fprint!(out, "T3Vvar(", t2v, ")")
//
| T3Vfix(t2v) =>
  fprint!(out, "T3Vfix(env)")
| T3Varg(t2v) =>
  fprint!(out, "T3Varg(arg)")
| T3Venv(t2v, ind) =>
  fprint!(out, "T3Venv(", ind, ")")
//
| T3Vfun _ => (temit3_T3Vfun(out, t3v))
//
)

(* ****** ****** *)

implement
temit3_ins
(out, ins) = let
//
(*
val () =
println!
("temit3_ins: ins = ", ins)
*)
//
in
(
case+ ins of
| T3Imov(r1, v2) =>
  (
    temit3(out, r1); 
    temit3(out, " = ");
    temit3(out, v2); 
    temit3(out, " ; ");
  )
| T3Ical(r1, v2, v3) =>
  (
    temit3(out, r1);
    temit3(out, " = ");
    temit3(out, "FUNCLO_APP("); temit3(out, v2); temit3(out, ", "); temit3(out, v3); temit3(out, ") ;");
  )
| T3Iift(v1, inss2, inss3) =>
  (
    temit3_str(out, "T3Iift");
    temit3_str(out, "("); temit3_val(out, v1); temit3_str(out, ")\n");
    temit3_str(out, "{\n");
    temit3_inss(out, inss2);
    temit3_str(out, "} else {\n");
    temit3_inss(out, inss3);
    temit3_str(out, "}\n");
  )
| _ => fprint!(out, ins, "; ")
)
end

(* ****** ****** *)

implement
temit3_inss
  (out, xs) =
(
case+ xs of
| list0_nil() => ()
| list0_cons(x0, xs) =>
  (temit3_ins(out, x0); temit3_eol(out); temit3_inss(out, xs))
)

(* ****** ****** *)
//
implement
temit3_T3Vfun
  (out, t3v0) = let
//
val-
T3Vfun
(t3f, t2vs, _, _) = t3v0
//
fun
auxlst0
(xs: t2varlst): void =
case+ xs of
| list0_nil() => ()
| list0_cons(x1, xs) => let
(*
    val () =
    println!
    ("auxlst0: x1 = ", x1.v2knd())
*)
  in
    if
    t2var_isarg(x1)
    then auxlst1(xs) else auxlst0(xs)
  end // end of [list0_cons]
//
and
auxlst1
(xs: t2varlst): void =
(
case+ xs of
| list0_nil() => ()
| list0_cons(x1, xs) =>
  (
    temit3_var(out, x1);
    if
    t2var_isarg(x1)
    then auxlst2(xs, 1) else auxlst1(xs)
  )
)
//
and
auxlst2
(xs: t2varlst, n: int): void =
(
case+ xs of
| list0_nil() => ()
| list0_cons(x1, xs) =>
  (
    temit3_str(out, "env");
    temit3_str(out, "[");
    temit3_int(out, n);
    temit3_str(out, "]");
    auxlst2(xs, n+1)
  )
)
//
in
//
  temit3_fun(out, t3f); temit3_str(out, "_funclo");
  temit3_str(out, "("); auxlst0(t2vs); temit3_str(out, ")");
//
end // end of [temit3_T3Vfun]
//
(* ****** ****** *)

implement
temit3_T3Vfun_dec
  (out, t3v0) = let
//
val-
T3Vfun
(t3f, t2vs, inss, ret) = t3v0
//
in
//
temit3_str
(out, "lamval\n");
//
temit3_fun(out, t3f);
//
temit3_str(out, "(");
temit3_str(out, "lamval arg");
temit3_str(out, " , ");
temit3_str(out, "lamval *env");
temit3_str(out, ")");
temit3_str(out, "\n");
//
temit3_str(out, "{\n");
//
temit3_inss(out, inss);
//
temit3_str(out, "return "); temit3_val(out, ret); temit3_str(out, " ;\n");
//
temit3_str(out, "}\n");
//
end // end of [temit3_T3Vfun_dec]

(* ****** ****** *)

(* end of [syntax3_emit.dats] *)
