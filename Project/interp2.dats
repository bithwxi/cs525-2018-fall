(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#define :: list0_cons

(* ****** ****** *)
//
#staload "./syntax1.dats"
#staload "./syntax2.dats"
//
(* ****** ****** *)

datatype value =
  | VALint of int
  | VALunit of ()
  | VALbool of bool
  | VALstring of string
  | VALclo of (t2erm, t2env)
  | VALfix of (t2erm, t2env)
  | VALtup of (value, value)
  | VALref of ( ref(value) )
  | VALlst of ( list0(value) )
  | VALarr of ( array0(value) )
  
where
t2env = list0( $tup(t2var, value) )

(* ****** ****** *)

extern
fun
print_value : (value) -> void
and
prerr_value : (value) -> void
extern
fun
fprint_value : (FILEref, value) -> void

overload print with print_value
overload prerr with prerr_value
overload fprint with fprint_value

(* ****** ****** *)

implement
print_value(t0) = 
fprint_value(stdout_ref, t0)
implement
prerr_value(t0) = 
fprint_value(stderr_ref, t0)

(* ****** ****** *)

local

implement
fprint_val<value> = fprint_value

in (* in-of-local *)

implement
fprint_value
  (out, v0) =
(
case+ v0 of
//
| VALint(x) =>
  fprint!
  (out, "VALint(", x, ")")
| VALunit() =>
  fprint!(out, "VALunit(", ")")
| VALbool(x) =>
  fprint!(out, "VALbool(", x, ")")
| VALstring(x) =>
  fprint!(out, "VALstring(", x, ")")
//
| VALclo _ => fprint!(out, "VALclo(...)")
| VALfix _ => fprint!(out, "VALfix(...)")
//
| VALtup(v1, v2) =>
  fprint!(out, "VALtup(", v1, "; ", v2, ")")
//
| VALref(ref) =>
  fprint!(out, "VALref[", ref_get_ptr(ref), "](", !ref, ")")
//
| VALlst(lst) =>
  fprint!(out, "VALlst(", lst, ")")
//
| VALarr(arr) =>
  fprint!(out, "VALarr[", array0_get_ref(arr), "](", arr, ")")
//
) (* end of [fprint_value] *)

end // end of [local]

(* ****** ****** *)

extern
fun
interp0(src: t2erm): value
and
interp_env(src: t2erm, env: t2env): value

(* ****** ****** *)

implement
interp0(src) =
interp_env(src, list0_nil())

(* ****** ****** *)

local
//
static
fun
aux_var:
(t2var, t2env) -> value
static
fun
aux_opr:
(t2erm, t2env) -> value
//
implement
aux_var(x0, xvs) = let
  val opt =
  list0_find_opt(xvs, lam(xv) => xv.0 = x0)
  val-~Some_vt(xv) = opt
in
  xv.1
end // end of [aux_var]
//
implement
aux_opr
(t0, env) = let
//
val-
T2Mopr(opr, ts) = t0
//
val vs =
list0_map<t2erm><value>
  (ts, lam(t) => interp_env(t, env))
//
in
//
ifcase
| opr = "+" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALint(i1+i2)
  end
| opr = "-" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALint(i1-i2)
  end
| opr = "*" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALint(i1*i2)
  end
//
| opr = "<" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 < i2)
  end
| opr = "<=" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 <= i2)
  end
| opr = ">" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 > i2)
  end
| opr = ">=" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 >= i2)
  end
| opr = "=" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 = i2)
  end
| opr = "!=" =>
  let
    val-
    VALint(i1)::VALint(i2)::nil0() = vs in VALbool(i1 != i2)
  end
//
| opr = "print" =>
  let
    val-
    VALint(i0)::nil0() = vs in VALunit() where { val () = print!(i0) }
  end
| opr = "prstr" =>
  let
    val-
    VALstring(s0)::nil0() = vs in VALunit() where { val () = print!(s0) }
  end
//
| _ (* else *) =>
    (prerrln!("lambda1: interp: aux_opr: opr = ", opr); exit(1))
//
end // end of [aux_opr]
//
in (* in-of-local *)

implement
interp_env(t0, env) = let
//
(*
val () =
println!("interp_env: t0 = ", t0)
*)
//
in
(
case+ t0 of
//
| T2Mint(i) => VALint(i)
| T2Mbool(b) => VALbool(b)
| T2Munit( ) => VALunit( )
| T2Mstring(s) => VALstring(s)
//
| T2Mvar(x) => aux_var(x, env)
//
| T2Mlam _ => VALclo(t0, env)
//
| T2Mapp(t1, t2) => let
    val v1 = interp_env(t1, env)
    val v2 = interp_env(t2, env)
  in
    case- v1 of
    | VALclo
      (t_fun, env_fun) => let
        val-T2Mlam(x, _, t_body) = t_fun
      in
        interp_env
        (t_body, cons0( $tup(x, v2), env_fun ))
      end
    | VALfix(t_fix, env_fun) => let
        val-T2Mfix(f, x, _, _, t_body) = t_fix
        val env_fun =
          list0_cons( $tup(f, v1), env_fun )
        val env_fun =
          list0_cons( $tup(x, v2), env_fun )
      in
        interp_env(t_body, env_fun)
      end
  end
//
| T2Mlet(x, t1, t2) => let
    val v1 = interp_env(t1, env)
  in
    interp_env
    (t2, cons0( $tup(x, v1), env ))
  end
//
| T2Mfix _ => VALfix(t0, env)
//
| T2Mift(t1, t2, t3) => let
    val v1 = interp_env(t1, env)
  in
    case- v1 of
    | VALbool(b) =>
      interp_env(ifval(b, t2, t3), env)
  end
//
| T2Mopr(opr, ts) => aux_opr(t0, env)
//
| T2Mtup(t1, t2) =>
  VALtup(v1, v2) where
  {
    val v1 = interp_env(t1, env)
    val v2 = interp_env(t2, env)
  }
//
| T2Mfst(t1) => v1 where
  {
    val-VALtup(v1, _) = interp_env(t1, env)
  }
| T2Msnd(t1) => v2 where
  {
    val-VALtup(_, v2) = interp_env(t1, env)
  }
//
| T2Mann(t1, T2) =>
  interp_env(t1, env)
//
| T2Mnil() => VALlst(nil0())
| T2Mcons(t1, t2) =>
  let
    val v1 = interp_env(t1, env)
    val v2 = interp_env(t2, env)
    val-VALlst(vs) = v2
  in
    VALlst(cons0(v1, vs))
  end
//
| T2Mhead(t1) =>
  let
    val v1 =
    interp_env(t1, env)
    val-VALlst(vs) = v1
  in
    list0_head_exn(vs)
  end
| T2Mtail(t1) =>
  let
    val v1 =
    interp_env(t1, env)
    val-VALlst(vs) = v1
  in
    VALlst(list0_tail_exn(vs))
  end
| T2Misnil(t1) =>
  let
    val v1 =
    interp_env(t1, env)
    val-VALlst(vs) = v1
  in
    VALbool(list0_is_nil(vs))
  end
//
| T2Mref_new(t1) =>
  let
    val v1 =
    interp_env(t1, env)
  in
    VALref(ref<value>(v1))
  end
//
| T2Mref_get(t1) =>
  let
    val v1 = interp_env(t1, env)
    val-VALref(r1) = v1
  in
    !r1
  end
//
| T2Mref_set(t1, t2) =>
  let
    val v1 = interp_env(t1, env)
    val v2 = interp_env(t2, env)
    val-VALref(r1) = v1 in !r1 := v2; VALunit()
  end
//
| T2Marr_new(t1, t2) =>
  let
    val v1 = interp_env(t1, env)
    val v2 = interp_env(t2, env)
    val-VALint(i1) = v1
    val a1 =
    array0_make_elt<value>(i1, v2)
  in
    VALarr(a1)
  end
//
| T2Marr_size(t1) =>
  let
    val v1 = interp_env(t1, env)
    val-VALarr(vs) = v1
  in
    VALint
    (sz2i(array0_get_size(vs)))
  end  
//
| T2Marr_get_at(t1, t2) =>
  let
    val v1 = interp_env(t1, env)
    val v2 = interp_env(t2, env)
    val-VALarr(a1) = v1
    val-VALint(i2) = v2 in
    array0_get_at(a1, i2)
  end
//
| T2Marr_set_at(t1, t2, t3) =>
  let
    val v1 = interp_env(t1, env)
    val v2 = interp_env(t2, env)
    val v3 = interp_env(t3, env)
    val-VALarr(a1) = v1
    val-VALint(i2) = v2
  in
    array0_set_at(a1, i2, v3); VALunit()
  end
//
) (* end of [interp_env] *)
//
end // end of [let]

end // end of [local]

(* ****** ****** *)

(* end of [interp2.dats] *)
