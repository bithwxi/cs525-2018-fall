(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#dynload "./tokeniz.dats"
#dynload "./syntax0.dats"
#dynload "./syntax1.dats"
#dynload "./syntax1_txyz.dats"
#dynload "./trans01.dats"
#dynload "./syntax2.dats"
#dynload "./syntax2_tvar.dats"
#dynload "./trans12.dats"
#dynload "./tinfer2.dats"
#dynload "./interp2.dats"
#dynload "./syntax3.dats"
#dynload "./syntax3_tfun.dats"
#dynload "./syntax3_treg.dats"
#dynload "./syntax3_comp.dats"
#dynload "./syntax3_emit.dats"

(* ****** ****** *)

#staload "./tokeniz.dats"
#staload "./syntax0.dats"
#staload "./syntax1.dats"
#staload "./syntax1_txyz.dats"
#staload "./trans01.dats"
#staload "./syntax2.dats"
#staload "./syntax2_tvar.dats"
#staload "./trans12.dats"
#staload "./tinfer2.dats"
#staload "./interp2.dats"
#staload "./syntax3.dats"
#staload "./syntax3_tfun.dats"
#staload "./syntax3_treg.dats"
#staload "./syntax3_comp.dats"
#staload "./syntax3_emit.dats"

(* ****** ****** *)

#include "./TEST/mylibs.dats"

(*
#include "./TEST/mytests.dats"
*)

(*
#include "./TEST/QueenPuzzle.dats"
*)

(* ****** ****** *)
//
val
TMadd1 =
parse_from_string
("lam (x: int) => lam (y: int) => (+ x y)")
val T1Madd1 = trans01(TMadd1)
val T2Madd1 = trans12(T1Madd1)
//
val () = println!("TMadd1 = ", TMadd1)
val () = println!("T1Madd1 = ", T1Madd1)
val () = println!("T2Madd1 = ", T2Madd1)
//
(* ****** ****** *)
//
val
TMfact0 =
parse_from_string
("fix f(x:int) => ift (> x 0) then (* x (f (- x 1))) else 1")
//
val T1Mfact0 = trans01(TMfact0)
val T2Mfact0 = trans12(T1Mfact0)
val T1Pfact0 = tinfer0(T2Mfact0)
//
val () = println!("TMfact0 = ", TMfact0)
val () = println!("T1Mfact0 = ", T1Mfact0)
val () = println!("T2Mfact0 = ", T2Mfact0)
val () = println!("T1Pfact0 = ", T1Pfact0)
val ((*void*)) =
println!("fact(10) = ", interp0(T2Mapp(T2Mfact0, T2Mint(10))))
//
val T3Mfact0 = tcomp3_inslst_val(T2Mfact0)
//
val ((*void*)) =
println! ("T3Mfact0.0 = ", T3Mfact0.0)
val ((*void*)) =
println! ("T3Mfact0.1 = ", T3Mfact0.1)
//
val
out = stdout_ref
val ((*void*)) =
temit3_T3Vfun
(out, T3Mfact0.1)
val ((*void*)) =
fprint_newline(out)
//
val ((*void*)) =
temit3_T3Vfun_dec
(out, T3Mfact0.1)
//
(* ****** ****** *)

val
TMfact1 =
parse_from_string
(
"\
lam (n:int) => \
  (fix loop(i:int) => \
   lam (r:int) => \
   ift (< i n) \
   then \
   let i1 = (+ i 1) in loop(i1)(* (i1) r) end \
   else r \
  )(0)(1) \
"
)
//
val T1Mfact1 = trans01(TMfact1)
val T2Mfact1 = trans12(T1Mfact1)
//
val () = println!("TMfact1 = ", TMfact1)
val () = println!("T1Mfact1 = ", T1Mfact1)
val () = println!("T2Mfact1 = ", T2Mfact1)
//
val T1Pfact1 = tinfer0(T2Mfact1)
val () = println!("T1Pfact1 = ", T1Pfact1)
//
val ((*void*)) =
println!("fact1(10) = ", interp0(T2Mapp(T2Mfact1, T2Mint(10))))
//
val T3Mfact1 = tcomp3_inslst_val(T2Mfact1)
//
val ((*void*)) =
println! ("T3Mfact1.0 = ", T3Mfact1.0)
val ((*void*)) =
println! ("T3Mfact1.1 = ", T3Mfact1.1)
//
val
out = stdout_ref
val ((*void*)) =
temit3_T3Vfun
(out, T3Mfact1.1)
val ((*void*)) =
fprint_newline(out)
//
val ((*void*)) =
temit3_T3Vfun_dec
(out, T3Mfact1.1)
//
(* ****** ****** *)

implement
main0() =
{
//
val () =
println!
("Hello from [lambda4]!")
//
(*
val
res =
tcomp3_inslst_val(T2Madd1)
val () = println! ("res.0 = ", res.0)
val () = println! ("res.1 = ", res.1)
*)
//
(*
val
res =
tcomp3_inslst_val(T2Mfact)
val () = println! ("res.0 = ", res.0)
val () = println! ("res.1 = ", res.1)
*)
//
(*
val
res =
tcomp3_inslst_val(T2Mfact1)
val () = println! ("res.0 = ", res.0)
val () = println! ("res.1 = ", res.1)
*)
//
(*
val
res =
tcomp3_inslst_val(T2Mfact2)
val () = println! ("res.0 = ", res.0)
val () = println! ("res.1 = ", res.1)
*)
//
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [lambda4.dats] *)
