(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#staload "./syntax1.dats"
#staload "./syntax2.dats"
//
(* ****** ****** *)

typedef
t2var = $rec
{ stamp= int
, v2knd= v2knd
, t1ype= ref(t1ypeopt)
} (* end of [t2var] *)

(* ****** ****** *)

absimpl t2var_type = t2var

(* ****** ****** *)

local

val
theStamp = ref<int>(0)
fun
theStamp_getinc() =
let
val n = !theStamp in !theStamp := n+1; n
end // end of [theStamp_getinc]

in (* in-of-local *)

implement
t2var_new(k0) = let
val stamp = theStamp_getinc()
in
$rec
{
stamp= stamp, v2knd= k0, t1ype=ref(None0)
} (* $rec *)
end // end of [t2var_new]

end // end of [local]

(* ****** ****** *)

implement
t2var_isfix(t2v) =
(
case+
t2v.v2knd of
| V2Kfix() => true
| _ (* non-fix *) => false
)

implement
t2var_isarg(t2v) =
(
case+
t2v.v2knd of
| V2Karg() => true
| _ (* non-arg *) => false
)

implement
t2var_islet(t2v) =
(
case+
t2v.v2knd of
| V2Klet _ => true
| _ (* non-let *) => false
)

(* ****** ****** *)

implement
t2var_get_v2knd(t2v) = t2v.v2knd

(* ****** ****** *)
//
implement
eq_t2var_t2var
  (t2v1, t2v2) =
  (t2v1.stamp = t2v2.stamp)
//
(* ****** ****** *)
//
implement
t2var_get_t1ype
  (t2v) = !(t2v.t1ype)
implement
t2var_set_t1ype
  (t2v, T) = !(t2v.t1ype) := Some0(T)
//
(* ****** ****** *)
//
implement
print_t2var(t2v) =
fprint_t2var(stdout_ref, t2v)
implement
prerr_t2var(t2v) =
fprint_t2var(stderr_ref, t2v)

implement
fprint_t2var
  (out, t2v) = let
//
  val S = t2v.stamp
  val opt = !(t2v.t1ype)
//
in
//
case+ opt of
| None0() =>
  (
    fprint!(out, "t2v(", S, ")")
  )
| Some0(T) =>
  (
    fprint!(out, "t2v(", S, ")[", T, "]")
  )
end // end of [fprint_t2var]

(* ****** ****** *)

(* end of [syntax2_tvar.dats] *)
