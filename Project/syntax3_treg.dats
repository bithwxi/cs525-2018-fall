(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#staload "./syntax1.dats"
#staload "./syntax2.dats"
#staload "./syntax3.dats"
//
(* ****** ****** *)

typedef
t3reg = $rec{ stamp= int }

(* ****** ****** *)

absimpl t3reg_type = t3reg

(* ****** ****** *)

local

val
theStamp = ref<int>(0)
fun
theStamp_getinc() =
let
val n = !theStamp in !theStamp := n+1; n
end // end of [theStamp_getinc]

in (* in-of-local *)

implement
t3reg_new() = let
  val stamp =
  theStamp_getinc() in $rec{ stamp= stamp }
end // end of [t3reg_new1]

implement
t3reg_get_stamp(r0) = r0.stamp

end // end of [local]

(* ****** ****** *)
//
implement
eq_t3reg_t3reg
  (r1, r2) =
  (r1.stamp = r2.stamp)
//
(* ****** ****** *)

implement
print_t3reg(r) =
fprint_t3reg(stdout_ref, r)
implement
prerr_t3reg(r) =
fprint_t3reg(stderr_ref, r)

implement
fprint_t3reg
  (out, r) = let
//
  val S = r.stamp
//
in
  fprint!(out, "R(", S, ")")
end // end of [fprint_t3reg]

(* ****** ****** *)

(* end of [syntax3_treg.dats] *)
