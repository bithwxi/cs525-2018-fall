
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

implement main0() = ()


extern
fun
from : int -> stream(int)

implement
from(n) = $delay
(
  stream_cons(n, from(n+1))
)

fun
sieve
(xs: stream(int)): stream(int) =
$delay
(
let
  val-stream_cons(p0, xs) = !xs
in
  stream_cons
  (p0, sieve(xs1)) where
  {
    val xs1 =
    stream_filter_cloref(xs, lam(x) => x mod p0 > 0)
  }
end
)

val thePrimes = sieve(from(2))

val () =
println!("thePrimes[0] = ", thePrimes[0])
val () =
println!("thePrimes[1] = ", thePrimes[1])
val () =
println!("thePrimes[2] = ", thePrimes[2])
