
#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

implement main0() = ()


extern
fun
from : int -> stream_vt(int)

implement
from(n) = $ldelay
(
  stream_vt_cons(n, from(n+1))
)

fun
sieve
(xs: stream_vt(int)): stream_vt(int) =
$ldelay
(
let
  val-
  ~stream_vt_cons(p0, xs) = !xs
in
  stream_vt_cons
  (p0, sieve(xs1)) where
  {
    val xs1 =
    xs.filter()(lam(x) => x mod p0 > 0)
  }
end
, ~(xs)
)

val thePrimes = sieve(from(2))

val () =
println!("thePrimes[100] = ", stream_vt_nth_exn(thePrimes, 100))
