(* ****** ****** *)

implement
subst(t0, x, t) =
(
case+ t0 of
//
| TMint _ => t0
| TMbool _ => t0
| TMstring _ => t0
//
| TMvar y =>
  if x = y then t else t0
//
| TMlam(y, t1) =>
  if x = y
  then t0 else TMlam(y, subst(t1, x, t))
  // end of [if]
//
| TMapp(t1, t2) =>
  TMapp(subst(t1, x, t), subst(t2, x, t))
//
| TMlet(y, t1, t2) =>
  TMlet(y, t1, t2) where
  {
    val t1 = subst(t1, x, t)
    val t2 =
      if x = y then t2 else subst(t2, x, t)
    // end of [val]
  }
//
| TMopr(opr, us) =>
  TMopr(opr, list0_map(us, lam(u) => subst(u, x, t)))
//
| TMift(t1, t2, t3) =>
  TMift(subst(t1, x, t), subst(t2, x, t), subst(t3, x, t))
| TMfix(f1, x2, t3) =>
  if
  (x = f1)
  then t0 else
    (if x = x2 then t0 else TMfix(f1, x2, subst(t3, x, t)))
  // end of [if]
//
)

(* ****** ****** *)
