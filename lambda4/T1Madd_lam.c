/*
// A basic runtime for lambda
*/

/* ****** ****** */

#include "runtime.h"

/* ****** ****** */

/*
add = lam(x) => lam(y) => x + y
*/

/* ****** ****** */

extern
lamval
F0(lamval arg, lamval *env);
extern
lamval
F1(lamval arg, lamval *env);

/* ****** ****** */

extern
lamval
F0_funclo();
extern
lamval
F1_funclo(lamval x);

/* ****** ****** */

lamval
F0_funclo()
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo;
  p0->fenv = malloc(1*sizeof(void*));
  p0->fenv[0] = &F0; return (lamval)p0;
}
lamval
F1_funclo(lamval x)
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo;
  p0->fenv = malloc(2*sizeof(void*));
  p0->fenv[0] = &F1; p0->fenv[1] = x; return (lamval)p0;
}

/* ****** ****** */

lamval
F0
(lamval n, lamval *env)
{
  return F1_funclo(n);
}


lamval
F1
(lamval y, lamval *env)
{
  lamval R0;
  R0 = T3Vopr_add(env[1], y); return R0;
}

/* ****** ****** */

int
main()
{
  lamval Rf, Rr;
  Rf = F0_funclo();
  Rr =
  FUNCLO_APP(FUNCLO_APP(Rf, T3Vint(10)), T3Vint(100));
  printf("add(10, 100) = %i\n", ((lamval_int)Rr)->data);
  return 0;
}

/* ****** ****** */

/* end of [T1Madd_lam.c] */
