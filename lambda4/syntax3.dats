(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#staload "./syntax1.dats"
#staload "./syntax2.dats"
//
(* ****** ****** *)

abstype t3fun_type = ptr
typedef t3fun = t3fun_type

abstype t3reg_type = ptr
typedef t3reg = t3reg_type

(* ****** ****** *)
//
extern fun t3fun_new(): t3fun
extern fun t3reg_new(): t3reg
//
(* ****** ****** *)
//
extern
fun
eq_t3fun_t3fun
(f1: t3fun, f2: t3fun): bool
overload = with eq_t3fun_t3fun
//
extern
fun
eq_t3reg_t3reg
(r1: t3reg, r2: t3reg): bool
overload = with eq_t3reg_t3reg
//
(* ****** ****** *)

extern
fun
print_t3fun : (t3fun) -> void
and
prerr_t3fun : (t3fun) -> void
extern
fun
fprint_t3fun : (FILEref, t3fun) -> void

overload print with print_t3fun
overload prerr with prerr_t3fun
overload fprint with fprint_t3fun

(* ****** ****** *)

extern
fun
print_t3reg : (t3reg) -> void
and
prerr_t3reg : (t3reg) -> void
extern
fun
fprint_t3reg : (FILEref, t3reg) -> void

overload print with print_t3reg
overload prerr with prerr_t3reg
overload fprint with fprint_t3reg

(* ****** ****** *)

datatype
t3val =
//
| T3Vint of int
| T3Vbool of bool
| T3Vunit of ()
| T3Vstring of string
//
| T3Vreg of t3reg
| T3Vvar of t2var
//
| T3Vfix of t2var
| T3Varg of t2var
| T3Venv of (t2var, int)
//
| T3Vfun of
  (t3fun, t2varlst, t3inslst, t3val)
//
// end of [t3val]

and t3ins =
| T3Imov of (t3reg, t3val)
| T3Ical of (t3reg, t3val, t3val)
| T3Iopr of (t3reg, t1opr, t3valist)
| T3Iift of (t3val, t3inslst, t3inslst)

where
t3valist = list0(t3val)
and
t3inslst = list0(t3ins)

(* ****** ****** *)

extern
fun
print_t3val : (t3val) -> void
and
prerr_t3val : (t3val) -> void
extern
fun
fprint_t3val : (FILEref, t3val) -> void

overload print with print_t3val
overload prerr with prerr_t3val
overload fprint with fprint_t3val

(* ****** ****** *)

extern
fun
print_t3ins : (t3ins) -> void
and
prerr_t3ins : (t3ins) -> void
extern
fun
fprint_t3ins : (FILEref, t3ins) -> void

overload print with print_t3ins
overload prerr with prerr_t3ins
overload fprint with fprint_t3ins

(* ****** ****** *)

implement
print_t3val(v0) = 
fprint_t3val(stdout_ref, v0)
implement
prerr_t3val(v0) = 
fprint_t3val(stderr_ref, v0)

implement
print_t3ins(x0) = 
fprint_t3ins(stdout_ref, x0)
implement
prerr_t3ins(x0) = 
fprint_t3ins(stderr_ref, x0)

(* ****** ****** *)

local

implement
fprint_val<t2var> = fprint_t2var
implement
fprint_val<t3val> = fprint_t3val
implement
fprint_val<t3ins> = fprint_t3ins

in (* in-of-local *)

implement
fprint_t3val
  (out, v0) =
(
case+ v0 of
//
| T3Vint(i0) =>
  fprint!(out, "T3Vint(", i0, ")")
| T3Vbool(b0) =>
  fprint!(out, "T3Vbool(", b0, ")")
| T3Vunit() => fprint!(out, "T3Vunit")
| T3Vstring(s0) =>
  fprint!(out, "T3Vstring(", s0, ")")
//
| T3Vreg(reg) =>
    fprint!(out, "T3Vreg(", reg, ")")
| T3Vvar(t2v) =>
    fprint!(out, "T3Vvar(", t2v, ")")
//
| T3Vfix(t2v) =>
  fprint!(out, "T3Vfix(", t2v, ")")
| T3Varg(t2v) =>
  fprint!(out, "T3Varg(", t2v, ")")
| T3Venv(t2v, n0) =>
  fprint!(out, "T3Venv(", t2v, "; ", n0, ")")
//
| T3Vfun
  (t3f, env, ins, t3v) =>
  fprint!
  (out, "T3Vfun(", t3f, "; ", env, "; ", ins, "; ", t3v, ")")
//
)

(* ****** ****** *)

implement
fprint_t3ins
  (out, i0) =
(
case+ i0 of
| T3Imov(r0, v0) =>
  (
    fprint!
    (out, "T3Imov(", r0, "; ", v0, ")")
  )
| T3Ical(r0, vf, va) =>
  (
    fprint!
    (out, "T3Ical(", r0, "; ", vf, "; ", va, ")")
  )
| T3Iopr(r0, nm, vs) =>
  (
    fprint!
    (out, "T3Iopr(", r0, "; ", nm, "; ", vs, ")")
  )
| T3Iift(v0, is1, is2) =>
  (
    fprint!
    (out, "T3Iift(", v0, "; ", is1, "; ", is2, ")")
  )
) (* end of [fprint_t3ins] *)

end // end of [local]


(* ****** ****** *)

(* end of [syntax3.dats] *)
