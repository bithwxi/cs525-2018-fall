(* ****** ****** *)
/*
HX:
For testing parcomb
*/
(* ****** ****** *)
//
local
#define MAIN_NONE
in(*in-of-local*)
#include "./tokeniz.dats"
end // end of [local]
//
(* ****** ****** *)
//
datatype type =
  | TPbas of string
  | TPfun of (type, type)
  | TPtup of (type, type)
//
typedef typeopt = option0(type)
//
(* ****** ****** *)

extern
fun
print_type : (type) -> void
and
prerr_type : (type) -> void
extern
fun
fprint_type : (FILEref, type) -> void

overload print with print_type
overload prerr with prerr_type
overload fprint with fprint_type

(* ****** ****** *)

implement
print_type(T0) = 
fprint_type(stdout_ref, T0)
implement
prerr_type(T0) = 
fprint_type(stderr_ref, T0)

implement
fprint_type
  (out, T0) =
(
case+ T0 of
| TPbas(nm) =>
  fprint!(out, "TPbas(", nm, ")")
| TPfun(T1, T2) =>
  fprint!(out, "TPfun(", T1, "; ", T2, ")")
| TPtup(T1, T2) =>
  fprint!(out, "TPtup(", T1, "; ", T2, ")")
)

(* ****** ****** *)

typedef tvar = string
typedef topr = string

(* ****** ****** *)
//
datatype targ = 
  | TAvar of tvar
  | TAann of (tvar, typeopt)
//
(* ****** ****** *)
//
extern
fun
print_targ : (targ) -> void
and
prerr_targ : (targ) -> void
extern
fun
fprint_targ : (FILEref, targ) -> void

overload print with print_targ
overload prerr with prerr_targ
overload fprint with fprint_targ

(* ****** ****** *)

implement
print_targ(ta) = 
fprint_targ(stdout_ref, ta)
implement
prerr_targ(ta) = 
fprint_targ(stderr_ref, ta)

local

implement
fprint_val<type> = fprint_type

in

implement
fprint_targ
  (out, ta) =
(
case+ ta of
| TAvar(nm) =>
  fprint!(out, "TAvar(", nm, ")")
| TAann(nm, opt) =>
  fprint!(out, "TAann(", nm, "; ", opt)
)

end

(* ****** ****** *)

datatype term =
//
  | TMint of (int)
//
  | TMvar of tvar
//
  | TMlam of (targ, term)
  | TMapp of (term, term)
//
  | TMopr of (topr, termlst)
//
  | TMfix of (tvar(*f*), targ(*x*), term)
//
  | TMift of (term, term, term) // if non-zero then ... else ...
//
  | TMann of (term, type(*annotation*))
//
where termlst = list0(term)

(* ****** ****** *)

fun
term_is_opr
  (t0: term): bool =
(
case+ t0 of
| TMvar(nm) =>
  (
   ifcase
   | nm = "+" => true
   | nm = "-" => true
   | nm = "*" => true
   | nm = "/" => true
   | nm = "<" => true
   | nm = ">" => true
   | nm = "=" => true
   | nm = "<=" => true
   | nm = ">=" => true
   | nm = "!=" => true
   | _ (*else*) => false
  )
| _(*non-TMvar*) => false
)

(* ****** ****** *)

fun
TMapplst
(
t0: term, ts: termlst
) : term =
(
if
term_is_opr(t0)
then
(
case- t0 of
| TMvar(nm) => TMopr(nm, ts)
)
else auxlst(t0, ts)
) where
{
fun
auxlst(t0: term, ts: termlst): term = 
(
case+ ts of
| list0_nil() => t0
| list0_cons(t1, ts) => auxlst(TMapp(t0, t1), ts)
)
}
//
(* ****** ****** *)

extern
fun
print_term : (term) -> void
and
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_term(t0) = 
fprint_term(stdout_ref, t0)
implement
prerr_term(t0) = 
fprint_term(stderr_ref, t0)

local

implement
fprint_val<term> = fprint_term

in (* in-of-local *)

implement
fprint_term
  (out, t0) =
(
case+ t0 of
//
| TMint(i1) =>
  fprint!(out, "TMint(", i1, ")")
//
| TMvar(x1) =>
  fprint!(out, "TMvar(", x1, ")")
//
| TMlam(x1, t2) =>
  fprint!
  (out, "TMlam(", x1, "; ", t2, ")")
| TMapp(t1, t2) =>
  fprint!
  (out, "TMapp(", t1, "; ", t2, ")")
//
| TMopr(nm, ts) =>
  fprint!(out, "TMopr(", nm, "; ", ts, ")")
//
| TMfix(f1, x2, t3) =>
  fprint!
  (out, "TMfix(", f1, "; ", x2, "; ", t3, ")")
//
| TMift(t1, t2, t3) =>
  fprint!
  (out, "TMift(", t1, "; ", t2, "; ", t3, ")")
//
| TMann(t1, T2) =>
  fprint!(out, "TMann(", t1, "; ", T2, ")")
//
) (* end of [fprint_term] *)

end // end of [local]

(* ****** ****** *)
//
extern
fun
toksat
( test
: token -<cloref1> bool
) : parser(token, token)
//
implement
toksat(test) =
sat_parser_cloref<token><token>
  (any_parser<token>(), test)
//
val
par_COMMA =
toksat
(
lam x =>
case+ x of
| TOKspc(',') => true | _ => false
)
val
par_COLON =
toksat
(
lam x =>
case+ x of
| TOKkwd(":") => true | _ => false
)
//
val
par_RPAREN =
toksat
(
lam x =>
case+ x of
| TOKspc(')') => true | _ => false
)
val
par_LPAREN =
toksat
(
lam x =>
case+ x of
| TOKspc('\(') => true | _ => false
)
//
val
par_IFT =
toksat
(
  lam x =>
  case+ x of
  | TOKkwd("ift") => true | _ => false
)
val
par_THEN =
toksat
(
  lam x =>
  case+ x of
  | TOKkwd("then") => true | _ => false
)
val
par_ELSE =
toksat
(
  lam x =>
  case+ x of
  | TOKkwd("else") => true | _ => false
)
//
(* ****** ****** *)
//
val
par_EQ =
toksat
(
  lam x =>
  case+ x of
  | TOKkwd("=") => true | _ => false
)
val
par_EQGT =
toksat
(
  lam x =>
  case+ x of
  | TOKkwd("=>") => true | _ => false
)
//
(* ****** ****** *)

val
par_FUN =
toksat
(
lam x =>
case+ x of
| TOKkwd("fun") => true | _ => false
)
val
par_TUP =
toksat
(
lam x =>
case+ x of
| TOKkwd("tup") => true | _ => false
)

(* ****** ****** *)
//
val
par_LAM =
toksat
(
lam x =>
case+ x of
| TOKkwd("lam") => true | _ => false
)
val
par_FIX =
toksat
(
lam x =>
case+ x of
| TOKkwd("fix") => true | _ => false
)
//
(* ****** ****** *)
//
val
par_LET =
toksat
(
lam x =>
case+ x of
| TOKkwd("let") => true | _ => false
)
val
par_IN =
toksat
(
lam x =>
case+ x of
| TOKkwd("in") => true | _ => false
)
val
par_END =
toksat
(
lam x =>
case+ x of
| TOKkwd("end") => true | _ => false
)
//
(* ****** ****** *)

//
extern
fun
{t:t0p}
{a1,a2:t0p}
par_tup2_fst
( p1: parser(t, a1)
, p2: parser(t, a2)): parser(t, a1)
extern
fun
{t:t0p}
{a1,a2:t0p}
par_tup2_snd
( p1: parser(t, a1)
, p2: parser(t, a2)): parser(t, a2)
//
overload << with par_tup2_fst
overload >> with par_tup2_snd
//
(* ****** ****** *)
//
implement
{t}{a1,a2}
par_tup2_fst(p1, p2) =
seq2wth_parser_fun<t><a1,a2,a1>
  (p1, p2, lam(x1, x2) => x1)
//
implement
{t}{a1,a2}
par_tup2_snd(p1, p2) =
seq2wth_parser_fun<t><a1,a2,a2>
  (p1, p2, lam(x1, x2) => x2)
//
(* ****** ****** *)

val
par_TPbas =
seq1wth_parser_fun<token><token,type>
(
toksat
(
  lam tok =>
  case+ tok of
  | TOKide _ => true
  | TOKsym _ => true | _ => false
)
,
lam(tok) =>
(
case- tok of
| TOKide(ide) => TPbas(ide) | TOKsym(sym) => TPbas(sym)
)
)

(* ****** ****** *)
//
extern
fun
lpar_type
(
// argless
) : lazy(parser(token, type))
//
val
par_type =
parser_unlazy(lpar_type())
//
val
par_TPfun =
seq2wth_parser_fun<token><type,type,type>
(
  par_FUN >> (par_LPAREN >> par_type)
, par_COMMA >> (par_type << par_RPAREN)
, lam(T1, T2) => TPfun(T1, T2)
)
val
par_TPtup =
seq2wth_parser_fun<token><type,type,type>
(
  par_TUP >> (par_LPAREN >> par_type)
, par_COMMA >> (par_type << par_RPAREN)
, lam(T1, T2) => TPtup(T1, T2)
)
//
implement
lpar_type() = $delay
(
  par_TPbas ||
  par_TPfun ||
  par_TPtup ||
  fail_parser<token><type>()
)
//
(* ****** ****** *)
//
val
par_tann =
option_parser<token><type>
(
  par_COLON >> par_type
)
//
(* ****** ****** *)

val
par_var =
seq1wth_parser_fun<token><token,tvar>
(
toksat
(
  lam tok =>
  case+ tok of
  | TOKide _ => true | _ => false
)
,
lam(tok) =>
let val-TOKide(name) = tok in name end
)

(* ****** ****** *)

val
par_TAvar =
seq1wth_parser_fun<token><tvar,targ>
  (par_var, lam(nm) => TAvar(nm))
val
par_TAann =
seq2wth_parser_fun<token><tvar,typeopt,targ>
(
par_LPAREN >> par_var
,
par_tann << par_RPAREN, lam(t1, T2) => TAann(t1, T2)
)

val
par_targ =
(
   par_TAvar
|| par_TAann
|| fail_parser<token><targ>()
)

(* ****** ****** *)

val
par_TMint =
seq1wth_parser_fun<token><token,term>
(
toksat
(
  lam tok =>
  case+ tok of
  | TOKint _ => true | _ => false
)
,
lam(tok) =>
let val-TOKint(i0) = tok in TMint(i0) end
)

val
par_TMvar =
seq1wth_parser_fun<token><token,term>
(
toksat
(
  lam tok =>
  case+ tok of
  | TOKide _ => true
  | TOKsym _ => true | _ => false
)
,
lam(tok) =>
(
case- tok of
| TOKide(ide) => TMvar(ide) | TOKsym(sym) => TMvar(sym)
)
)

(* ****** ****** *)
//
extern
fun
lpar_aterm
(
// argless
) : lazy(parser(token, term))
extern
fun
lpar_term0
(
// argless
) : lazy(parser(token, term))
//
(* ****** ****** *)

val
par_aterm =
parser_unlazy(lpar_aterm())
val
par_term0 =
parser_unlazy(lpar_term0())

(* ****** ****** *)

val
par_TMlam =
seq2wth_parser_fun<token><targ,term,term>
(
  par_LAM >> par_targ
, par_EQGT >> par_term0
, lam(x, t) => TMlam(x, t)
)

(* ****** ****** *)
//
val
par_TMfix =
seq3wth_parser_fun<token><tvar,targ,term,term>
(
  par_FIX >> par_var
, par_targ
, par_EQGT >> par_term0
, lam(f, x, t) => TMfix(f, x, t)
)
//
(* ****** ****** *)

val
par_TMift =
seq3wth_parser_fun<token><term,term,term,term>
(
  par_IFT >> par_term0
, par_THEN >> par_term0
, par_ELSE >> par_term0
, lam(t0, t1, t2) => TMift(t0, t1, t2)
)

(* ****** ****** *)

val
par_TMapplst =
seq2wth_parser_fun<token><term,termlst,term>
(
  par_aterm,
  list0_parser(par_aterm)
, lam(t0, ts) => TMapplst(t0, ts)
)
val
par_TMapplst_ann =
seq2wth_parser_fun<token><term,typeopt,term>
(
  par_TMapplst, par_tann
, lam(t0, opt) =>
  (case+ opt of None0() => t0 | Some0(T0) => TMann(t0, T0))
)
(* ****** ****** *)

implement
lpar_aterm() = $delay
(
par_TMint ||
par_TMvar ||
(par_LPAREN >> (par_term0 << par_RPAREN))
)

(* ****** ****** *)

implement
lpar_term0() = $delay
(
  par_TMlam ||
  par_TMfix ||
  par_TMift ||
  par_TMapplst_ann || 
  fail_parser<token><term>()
)

(* ****** ****** *)

fun
cstream_eof
(
cs: stream(int)
) : stream(int) = $delay
(
case+ !cs of
| stream_nil
    () => stream_sing(~1)
  // end of [stream_nil]
| stream_cons
    (c0, cs) => stream_cons(c0, cstream_eof(cs))
  // end of [stream_cons]
)

(* ****** ****** *)

fun
token_is_space
  .<>.
  (t0: token):<> bool =
(
case+ t0 of
| TOKspc(c0) => isspace(c0) | _ => false
)

fun
tstream_despc_eof
(
ts: stream(token)
) : stream(token) = $delay
(
case+ !ts of
| stream_nil
    () => stream_sing(TOKeof)
| stream_cons
    (t0, ts) =>
  (
    if
    token_is_space(t0)
    then !(tstream_despc_eof(ts))
    else stream_cons(t0, tstream_despc_eof(ts))
    // end of [if]
  )
)

(* ****** ****** *)
//
extern
fun
parse_from_string(inp: string): term
extern
fun
parse_from_fileref(inp: FILEref): term
//
extern
fun
parse_from_cstream(cs: stream(char)): term
//
(* ****** ****** *)

implement
parse_from_string
  (cs) =
( parse_from_cstream
  (stream_vt2t(streamize_string_char(cs)))
)
implement
parse_from_fileref
  (inp) =
( parse_from_cstream
  (stream_vt2t(streamize_fileref_char(inp)))
)

(* ****** ****** *)

implement
parse_from_cstream
  (cs) = let
//
val cs =
stream_map_cloref
(cs, lam(c) => char2int0(c))
//
val cs = cstream_eof(cs)
//
val pt0 =
token_parser
(any_parser<int>((*void*)))
//
val pts = list0_parser(pt0)
//
val tks =
  parser_apply_stream(pts, cs)
//
(*
val () = println!("tokens = ", tks)
*)
//
val tks =
  streamize_list0_elt(tks)
val tks = stream_vt2t(tks)
val tks = tstream_despc_eof(tks)
//
in
  parser_apply_stream(par_term0, tks)
end // end of [parser_from_string]

(* ****** ****** *)

(* end of [syntax0.dats] *)
