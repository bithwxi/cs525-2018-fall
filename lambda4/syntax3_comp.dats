(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#staload "./syntax1.dats"
#staload "./syntax2.dats"
#staload "./syntax3.dats"
//
(* ****** ****** *)
//
abstype stack = ptr
//
(* ****** ****** *)

local

absimpl
stack = ref(list0(t3inslst))

in (* in-of-local *)

fun
stack_new
  (): stack = ref(list0_nil)

fun
stack_ins
(stack: stack, x0: t3ins) =
{
//
  val-
  list0_cons
  (xs0, xss) = !stack
  val () =
  !stack :=
    list0_cons
    (list0_cons(x0, xs0), xss)
//
} (* end of [stack_ins] *)

fun
stack_popins
  (stack: stack): t3inslst =
  xs0 where
{
  val-
  list0_cons
  (xs0, xss) = !stack
  val () = !stack := xss
  val xs0 = list0_reverse(xs0)
}

fun
stack_push
  (stack: stack): void =
(
  !stack :=
  list0_cons(list0_nil, !stack)
)

end // end of [local]

(* ****** ****** *)
//
extern
fun{}
tcomp3_stack(): stack
//
(* ****** ****** *)
//
extern
fun{}
tcomp3_push(): void
extern
fun{}
tcomp3_popins(): t3inslst
//
(* ****** ****** *)

extern
fun{}
tcomp3_ins(i0: t3ins): void

(* ****** ****** *)
//
implement
tcomp3_push<>() =
  stack_push(tcomp3_stack<>())
implement
tcomp3_popins<>() =
  stack_popins(tcomp3_stack<>())
//
(* ****** ****** *)
//
implement
tcomp3_ins<>(ins) =
  stack_ins(tcomp3_stack<>(), ins)
//
(* ****** ****** *)
//
extern
fun
tcomp3_isf
  (xs: t2varlst, x0: t2var): bool
extern
fun
tcomp3_isa
  (xs: t2varlst, x0: t2var): bool
extern
fun
tcomp3_ise
  (xs: t2varlst, x0: t2var): bool
//
(* ****** ****** *)
//
implement
tcomp3_isf
  (xs, x0) =
(
case+ xs of
| list0_nil() => false
| list0_cons(x, xs) =>
  if t2var_isf(x) then x = x0 else false
)
//
implement
tcomp3_isa
  (xs, x0) =
(
  auxlst1(xs)
) where
{
  fun
  auxlst1
  (xs: t2varlst): bool =
  (
  case xs of
  | list0_nil() => false
  | list0_cons(x, xs) =>
    if
    t2var_isa(x)
    then (x = x0) else auxlst1(xs)
  )
}
//
implement
tcomp3_ise
  (xs, x0) =
(
  auxlst1(xs)
) where
{
  fun
  auxlst1
  (xs: t2varlst): bool =
  (
  case xs of
  | list0_nil() => false
  | list0_cons(x, xs) =>
    if
    t2var_isa(x)
    then auxlst2(xs) else auxlst1(xs)
  )
  and
  auxlst2
  (xs: t2varlst): bool =
  (
  case xs of
  | list0_nil() => false
  | list0_cons(x, xs) =>
    if x = x0 then true else auxlst2(xs)
  )
}
//
(* ****** ****** *)
//
extern
fun
tcomp3_var
(xs: t2varlst, x0: t2var): t3val
//
(* ****** ****** *)

implement
tcomp3_var(xs, x0) =
ifcase
| tcomp3_isf(xs, x0) => T3Vfix(x0)
| tcomp3_isa(xs, x0) => T3Varg(x0)
| tcomp3_ise(xs, x0) => T3Venv(x0, 0)
| _ (* non-arg-env *) => T3Vvar(x0)

(* ****** ****** *)
//
extern
fun{}
tcomp3_main
(t2vs: t2varlst, t0: t2erm): t3val
//
(* ****** ****** *)

implement
{}(*tmp*)
tcomp3_main
(xs, t0) =
tcomp3_main
  (xs, t0) where
{
fun
tcomp3_main
(
xs: t2varlst, t0: t2erm
) : t3val = let
//
// (*
val () =
println!("tcomp3: t0 = ", t0)
// *)
//
in
//
case- t0 of
//
| T2Mint(i) => T3Vint(i)
| T2Mbool(b) => T3Vbool(b)
| T2Munit( ) => T3Vunit( )
//
| T2Mstring(s) => T3Vstring(s)
//
| T2Mvar(x0) => tcomp3_var(xs, x0)
//
| T2Mlam
  (x1, opt, t2) =>
  let
    val tf = t3fun_new()
    val () = tcomp3_push()
    val xs = list0_cons(x1, xs)
    val vf = tcomp3_main(xs, t2)
    val is = tcomp3_popins()
  in
    T3Vfun(tf, xs, is, vf)
  end
//
| T2Mapp(t1, t2) => let
    val r0 = t3reg_new()
    val v1 = tcomp3_main(xs, t1)
    val v2 = tcomp3_main(xs, t2)
  in
    tcomp3_ins(T3Ical(r0, v1, v2)); T3Vreg(r0)
  end
//
| T2Mopr(nm, ts) => let
    val r0 = t3reg_new()
    val vs =
    list0_map
    ( ts
    , lam(t) => tcomp3_main(xs, t))
  in
    tcomp3_ins(T3Iopr(r0, nm, vs)); T3Vreg(r0)
  end // end of [T2Mopr]
//
| T2Mift(t1, t2, t3) => let
    val r0 = t3reg_new()
    val v1 = tcomp3_main(xs, t1)
//
    val () = tcomp3_push()
    val v2 = tcomp3_main(xs, t2)
    val () = tcomp3_ins(T3Imov(r0, v2))
    val is2 = tcomp3_popins()
//
    val () = tcomp3_push()
    val v3 = tcomp3_main(xs, t3)
    val () = tcomp3_ins(T3Imov(r0, v3))
    val is3 = tcomp3_popins()
  in
    tcomp3_ins(T3Iift(v1, is2, is3)); T3Vreg(r0)
  end
//
| T2Mfix
  (f1, x2, _, _, t3) =>
  let
    val tf = t3fun_new()
    val () = tcomp3_push()
    val xs = list0_cons(x2, xs)
    val xs = list0_cons(f1, xs)
    val vf = tcomp3_main(xs, t3)
    val is = tcomp3_popins() in T3Vfun(tf, xs, is, vf)
  end // end of [T2Mfix]
//
| T2Mann(t1, T2) => tcomp3_main(xs, t1)
//
end // end of [tcomp3_main]
//
} (* end of [implement tcomp3_main] *)

(* ****** ****** *)

extern
fun
tcomp3_inslst_val(t2erm): $tup(t3inslst, t3val)

(* ****** ****** *)

implement
tcomp3_inslst_val
  (t0) = let
//
local
val
stack = stack_new()
in
implement
tcomp3_stack<>() = stack
end // end of [local]
//
val () = tcomp3_push()
val v0 =
(
  tcomp3_main(t2vs, t0)
) where
{
  val t2vs = list0_nil()
}
val xs = tcomp3_popins()
//
in
  $tup(xs, v0)
end // end of [tcomp3_inslst_val]

(* ****** ****** *)

(* end of [syntax3_comp.dats] *)
