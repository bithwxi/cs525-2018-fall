(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

val r0 = ref<int>(0)

val () = println!("!r0 = ", !r0)

val () = println!("r0[] = ", r0[])

val () = !r0 := !r0 + 1

val () = println!("r0[] = ", r0[])

val () = r0[] := r0[] + 2

val () = println!("r0[] = ", r0[])

implement main0() = {}
