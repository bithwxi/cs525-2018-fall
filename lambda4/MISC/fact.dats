
#include
"share/atspre_staload.hats"

extern
fun
app2 :
((int, int) -<cloref1> int, int, int) -> int = "app2"

fun
fact(n: int): int = let
//
fun
loop(i: int, res: int):<cloref1> int =
  if i < n then loop(i+1, res * (i+1)) else res
//
in
  app2(loop, 0, 1)
end