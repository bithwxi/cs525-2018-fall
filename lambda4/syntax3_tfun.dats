(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)
//
#staload "./syntax1.dats"
#staload "./syntax2.dats"
#staload "./syntax3.dats"
//
(* ****** ****** *)

typedef
t3fun = $rec{ stamp= int }

(* ****** ****** *)

absimpl t3fun_type = t3fun

(* ****** ****** *)

local

val
theStamp = ref<int>(0)
fun
theStamp_getinc() =
let
val n = !theStamp in !theStamp := n+1; n
end // end of [theStamp_getinc]

in (* in-of-local *)

implement
t3fun_new() = let
  val stamp =
  theStamp_getinc() in $rec{ stamp= stamp }
end // end of [t3fun_new1]

end // end of [local]

(* ****** ****** *)
//
implement
eq_t3fun_t3fun
  (r1, r2) =
  (r1.stamp = r2.stamp)
//
(* ****** ****** *)

implement
print_t3fun(r) =
fprint_t3fun(stdout_ref, r)
implement
prerr_t3fun(r) =
fprint_t3fun(stderr_ref, r)

implement
fprint_t3fun
  (out, r) = let
//
  val S = r.stamp
//
in
  fprint!(out, "F(", S, ")")
end // end of [fprint_t3fun]

(* ****** ****** *)

(* end of [syntax3_tfun.dats] *)
