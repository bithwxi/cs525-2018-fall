(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#dynload "./tokeniz.dats"
#dynload "./syntax0.dats"
#dynload "./syntax1.dats"
#dynload "./syntax1_txyz.dats"
#dynload "./syntax2.dats"
#dynload "./syntax2_tvar.dats"
#dynload "./trans12.dats"
#dynload "./tinfer2.dats"
#dynload "./interp2.dats"
#dynload "./syntax3.dats"
#dynload "./syntax3_tfun.dats"
#dynload "./syntax3_treg.dats"
#dynload "./syntax3_comp.dats"

(* ****** ****** *)

#staload "./tokeniz.dats"
#staload "./syntax0.dats"
#staload "./syntax1.dats"
#staload "./syntax1_txyz.dats"
#staload "./syntax2.dats"
#staload "./syntax2_tvar.dats"
#staload "./trans12.dats"
#staload "./tinfer2.dats"
#staload "./interp2.dats"
#staload "./syntax3.dats"
#staload "./syntax3_tfun.dats"
#staload "./syntax3_treg.dats"
#staload "./syntax3_comp.dats"

(* ****** ****** *)

#include "./mylibs.dats"
#include "./mytests.dats"
// #include "./QueenPuzzle.dats"

(* ****** ****** *)

implement
main0() =
{
//
val () =
println!
("Hello from [lambda4]!")
//
val TMtest1 =
parse_from_string
("lam x => (+ x x: int)")
val ((*void*)) =
println! ("TMtest1 = ", TMtest1)
//
val
res =
tcomp3_inslst_val(T2Madd)
val () = println! ("res.0 = ", res.0)
val () = println! ("res.1 = ", res.1)
//
(*
val
res =
tcomp3_inslst_val(T2Mfact)
val () = println! ("res.0 = ", res.0)
val () = println! ("res.1 = ", res.1)
*)
//
(*
val
res =
tcomp3_inslst_val(T2Mfact1)
val () = println! ("res.0 = ", res.0)
val () = println! ("res.1 = ", res.1)
*)
//
(*
val
res =
tcomp3_inslst_val(T2Mfact2)
val () = println! ("res.0 = ", res.0)
val () = println! ("res.1 = ", res.1)
*)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [lambda4.dats] *)
