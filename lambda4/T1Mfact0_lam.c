/* ****** ****** */
/*
// A basic runtime for lambda
*/
/* ****** ****** */

#include "runtime.h"

/* ****** ****** */

extern
lamval
F0_funclo();

/*

fact(n) =
if n >= 1 then n * fact(n-1) else 1

*/

lamval
F0(lamval arg, lamval *env)
{
  lamval R0, R1, R2, R3, R4, Rf;
  R1 = T3Vopr_gte(arg, T3Vint(1));
  T3Iift(R1)
  {
    R4 = T3Vopr_sub(arg, T3Vint(1));
    /*
    Rf = F0_funclo();
    R3 = FUNCLO_APP(Rf, R4);
    */
    R3 = F0(R4, env);
    R2 = T3Vopr_mul(arg, R3);
    R0 = R2;
  } else {
    R0 = T3Vint(1);
  }
  return R0;
}

lamval
F0_funclo()
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo;
  p0->fenv = malloc(1*sizeof(void*));
  p0->fenv[0] = &F0; return (lamval)p0;
}

/* ****** ****** */

int
main()
{
  lamval Rf, Rr;
  Rf = F0_funclo();
  Rr = FUNCLO_APP(Rf, T3Vint(10));
  printf("fact(10) = %i\n", ((lamval_int)Rr)->data);
  return 0;
}

/* ****** ****** */

/* end of [T1Mfact0_lam.c] */
