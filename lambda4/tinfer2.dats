(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#define :: list0_cons

(* ****** ****** *)
//
#staload "./syntax1.dats"
#staload "./syntax2.dats"
//
(* ****** ****** *)
local

val
theSigma =
g0ofg1
(
$list{$tup(t1opr, ct1ype)}
(
$tup("+", CT1P(T1Pint :: T1Pint :: nil0(), T1Pint))
,
$tup("-", CT1P(T1Pint :: T1Pint :: nil0(), T1Pint))
,
$tup("*", CT1P(T1Pint :: T1Pint :: nil0(), T1Pint))
,
$tup("=", CT1P(T1Pint :: T1Pint :: nil0(), T1Pbool))
,
$tup("!=", CT1P(T1Pint :: T1Pint :: nil0(), T1Pbool))
,
$tup("<", CT1P(T1Pint :: T1Pint :: nil0(), T1Pbool))
,
$tup(">", CT1P(T1Pint :: T1Pint :: nil0(), T1Pbool))
,
$tup("<=", CT1P(T1Pint :: T1Pint :: nil0(), T1Pbool))
,
$tup(">=", CT1P(T1Pint :: T1Pint :: nil0(), T1Pbool))
,
$tup("print", CT1P(T1Pint :: nil0(), T1Punit))
,
$tup("prstr", CT1P(T1Pstring :: nil0(), T1Punit))
)
)
typedef Sigma_t = list0($tup(t1opr, ct1ype))

in
//
extern
fun
tfind_opr: (t1opr) -> ct1ype
//
implement
tfind_opr(opr) =
loop(theSigma) where
{
fun
loop(kxs: Sigma_t): ct1ype =
(
case-
kxs of
| kx :: kxs =>
  if kx.0 = opr then kx.1 else loop(kxs)
)
}
//
end // end of [local]
//
(* ****** ****** *)

extern
fun
t1punopt: (t1ypeopt) -> t1ype
//
implement
t1punopt(OT) =
(
case+ OT of
| None0() => T1Pvar(t1xyz_new()) | Some0(T) => T
)

(* ****** ****** *)

extern
fun tinfer0 : t2erm -> t1ype

(* ****** ****** *)
//
extern
fun
tcsolve: (t1ype, t1ype) -> void
and
tcsolvelst: (t1ypelst, t1ypelst) -> void
//
(* ****** ****** *)
//
extern
fun
occursck: (t1xyz, t1ype) -> bool
//
(* ****** ****** *)
//
implement
occursck(X0, T0) =
(
case+ T0 of
| T1Pbas _ => false
| T1Pvar(X1) =>
  let
    val opt = X1.t1ype()
  in
    case+ opt of
    | None0() => X0 = X1
    | Some0(T1) => occursck(X1, T1)
  end
| T1Pfun(T1, T2) =>
  (occursck(X0, T1) || occursck(X0, T2))
| T1Ptup(T1, T2) =>
  (occursck(X0, T1) || occursck(X0, T2))
//
| T1Plst(T1) => occursck(X0, T1)
//
| T1Pref(T1) => occursck(X0, T1)
| T1Parr(T1) => occursck(X0, T1)
//
)
(* ****** ****** *)

implement
tinfer0(t0) = let
//
(*
val () =
println! ("tinfer0: t0 = ", t0)
*)
//
in
(
case+ t0 of
//
| T2Mint _ => T1Pint
| T2Mbool _ => T1Pbool
| T2Mstring _ => T1Pstring
//
| T2Munit() => T1Punit
//
| T2Mvar(t2v) => let
    val opt =
    t2var_get_t1ype(t2v)
  in
    case- opt of
    | Some0(T0) => T0
(*
    | None0((*void*)) => let
        val X0 = t1xyz_new()
        val T0 = T1Pvar(X0)
        val () = t2var_set_t1ype(t2v, T0) in T0
      end (* end of [None] *)
*)
  end
//
| T2Mlam
  (t2v, OT, t2) =>
  T1Pfun(T1, T2) where
  {
    val T1 =
      t1punopt(OT)
    val () =
      t2var_set_t1ype(t2v, T1)
    val T2 = tinfer0(t2)
  }
//
| T2Mapp(t1, t2) => T3 where
  {
    val T1 = tinfer0(t1)
    val T2 = tinfer0(t2)
    val T3 = T1Pvar(t1xyz_new())
    val () = tcsolve(T1, T1Pfun(T2, T3))
  }
//
| T2Mfix
  (f1, x2, T1, T2, t3) =>
  Tf where
  {
     val T1 = t1punopt(T1)
     val T2 = t1punopt(T2)
     val Tf = T1Pfun(T1, T2)
     val () = t2var_set_t1ype(f1, Tf)
     val () = t2var_set_t1ype(x2, T1)
     val T3 = tinfer0(t3)
     val () = tcsolve(T2, T3)
  }
//
| T2Mopr(opr, ts) => let
    val+
    CT1P(TS1, T0) = tfind_opr(opr)
    val TS2 = list0_map(ts, lam(t) => tinfer0(t))
  in
    tcsolvelst(TS1, TS2); T0
  end
//
| T2Mlet(x1, t1, t2) => let
    val T1 = tinfer0(t1)
    val () = t2var_set_t1ype(x1, T1)
  in
    tinfer0(t2)
  end
//
| T2Mift(t1, t2, t3) =>
  T2 where
  {
    val T1 = tinfer0(t1)
    val T2 = tinfer0(t2)
    val T3 = tinfer0(t3)
    val () = tcsolve(T1, T1Pbool)
    val () = tcsolve(T2, T3)
  }
//
| T2Mtup(t1, t2) =>
  let
    val T1 = tinfer0(t1)
    val T2 = tinfer0(t2)
  in
    T1Ptup(T1, T2)
  end
| T2Mfst(t1) =>
  T11 where
  {
    val T1 = tinfer0(t1)
    val X1 = t1xyz_new()
    val X2 = t1xyz_new()
    val T11 = T1Pvar(X1)
    val T12 = T1Pvar(X2)
    val () = tcsolve(T1, T1Ptup(T11, T12))
  }
| T2Msnd(t1) =>
  T12 where
  {
    val T1 = tinfer0(t1)
    val X1 = t1xyz_new()
    val X2 = t1xyz_new()
    val T11 = T1Pvar(X1)
    val T12 = T1Pvar(X2)
    val () = tcsolve(T1, T1Ptup(T11, T12))
  }
//
| T2Mann(t1, T2) =>
  T2 where
  {
    val T1 = tinfer0(t1)
    val () = tcsolve(T1, T2)
  }  
//
| T2Mnil() =>
  T1Plst(T1) where
  {
    val T1 = T1Pvar(t1xyz_new())
  }
| T2Mcons(t1, t2) =>
  T2 where
  {
    val T1 = tinfer0(t1)
    val T2 = tinfer0(t2)
    val () = tcsolve(T1Plst(T1), T2)
  }
//
| T2Mhead(t1) =>
  T2 where
  {
    val T1 = tinfer0(t1)
    val T2 = T1Pvar(t1xyz_new())
    val () = tcsolve(T1, T1Plst(T2))
  } 
| T2Mtail(t1) =>
  T1 where
  {
    val T1 = tinfer0(t1)
    val T2 = T1Pvar(t1xyz_new())
    val () = tcsolve(T1, T1Plst(T2))
  } 
| T2Misnil(t1) =>
  T1Pbool where
  {
    val T1 = tinfer0(t1)
    val T2 = T1Pvar(t1xyz_new())
    val () = tcsolve(T1, T1Plst(T2))
  } 
//
| T2Mref_new(t1) =>
  T1Pref(T1) where
  {
    val T1 = tinfer0(t1)
  }
| T2Mref_get(t1) => let
    val T1 = tinfer0(t1)
    val T2 = T1Pvar(t1xyz_new())
    val () = tcsolve(T1, T1Pref(T2))
  in
    T2
  end
| T2Mref_set(t1, t2) => let
    val T1 = tinfer0(t1)
    val T2 = tinfer0(t2)
    val () = tcsolve(T1, T1Pref(T2))
  in
    T1Punit(*void*) // HX: for unit type
  end
//
| T2Marr_new(t1, t2) => let
    val T1 = tinfer0(t1)
    val T2 = tinfer0(t2)
    val () = tcsolve(T1, T1Pint)
  in
    T1Parr(T2)
  end
| T2Marr_size(t1) => let
    val T1 = tinfer0(t1)
    val T2 = T1Pvar(t1xyz_new())
    val () = tcsolve(T1, T1Parr(T2))
  in
    T1Pint
  end
| T2Marr_get_at(t1, t2) => let
    val T1 = tinfer0(t1)
    val T2 = tinfer0(t2)
    val T3 = T1Pvar(t1xyz_new())
    val () = tcsolve(T1, T1Parr(T3))
    val () = tcsolve(T2, T1Pint)
  in
    T3
  end
| T2Marr_set_at(t1, t2, t3) => let
    val T1 = tinfer0(t1)
    val T2 = tinfer0(t2)
    val T3 = tinfer0(t3)
    val () = tcsolve(T1, T1Parr(T3))
    val () = tcsolve(T2, T1Pint)
  in
    T1Punit(*void*) // HX: for unit type
  end
//
(* ****** ****** *)
//
)
end // end of [tinfer0]

(* ****** ****** *)

fun
t1pnorm(T0: t1ype): t1ype =
(
case+ T0 of
| T1Pvar(X0) => let
    val opt =
    t1xyz_get_t1ype(X0)
  in
    case+ opt of
    | None0() => T0
    | Some0(T1) =>
      T1 where
      {
        val T1 = t1pnorm(T1)
        val () =
        t1xyz_set_t1ype(X0, T1)
      }
  end // end of [T1Pvar]
| _(* non-T1Pvar *) => T0
)

(* ****** ****** *)

exception
Failure_occursck of ()

(* ****** ****** *)

implement
tcsolve(T1, T2) =
(
aux(t1pnorm(T1), t1pnorm(T2))
) where
{

fun
aux
( T1: t1ype
, T2: t1ype): void = let
//
(*
val () =
println! ("tcsolve: aux: T1 = ", T1)
val () =
println! ("tcsolve: aux: T2 = ", T2)
*)
//
in
(
case- (T1, T2) of
| (T1Pvar(X1), _) =>
  (
    auxvar(X1, T2)
  )
| (_, T1Pvar(X2)) =>
  (
    auxvar(X2, T1)
  )
//
| ( T1Pbas nm1,
    T1Pbas nm2 ) =>
    if
    (nm1 = nm2)
    then ()
    else (
      println!("tcsolve: aux: T1Pbas: nm1 = ", nm1);
      println!("tcsolve: aux: T1Pbas: nm2 = ", nm2);
      exit(1)
    )
//
| ( T1Pfun(T11, T12)
  , T1Pfun(T21, T22)) =>
  {
    val () = tcsolve(T11, T21)
    val () = tcsolve(T12, T22)
  }
| ( T1Ptup(T11, T12)
  , T1Ptup(T21, T22)) =>
  {
    val () = tcsolve(T11, T21)
    val () = tcsolve(T12, T22)
  }
//
| (T1Plst(T11), T1Plst(T21)) =>
  {
    val () = tcsolve(T11, T21)
  }
| (T1Pref(T11), T1Pref(T21)) =>
  {
    val () = tcsolve(T11, T21)
  }
| (T1Parr(T11), T1Parr(T21)) =>
  {
    val () = tcsolve(T11, T21)
  }
//
)
end

and
auxvar
( X1: t1xyz
, T2: t1ype): void =
(
case+ T2 of
| T1Pvar(X2) =>
  if
  X1 = X2
  then ((*void*))
  else t1xyz_set_t1ype(X1, T2)
| _(*non-T1Pvar*) =>
  if
  occursck(X1, T2)
  then
  (
  $raise Failure_occursck()
  )
  else t1xyz_set_t1ype(X1, T2)
)

} (* end of [tcsolve] *)

(* ****** ****** *)

implement
tcsolvelst
  (TS1, TS2) =
(
case-
(TS1, TS2) of
| (nil0(), nil0()) => ()
| (T1 :: TS1, T2 :: TS2) =>
  (tcsolve(T1, T2); tcsolvelst(TS1, TS2))
)

(* ****** ****** *)

(* end of [tinfer2.dats] *)
