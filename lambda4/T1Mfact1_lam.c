/*
// A basic runtime for lambda
*/

/* ****** ****** */

#include "runtime.h"

/* ****** ****** */

/*
fact=
lam(n) =>
  (fix loop(i) => lam (res) => if i < n then loop(i+1, (i+1)*res) else res)(0)(1)
*/

/* ****** ****** */

extern
lamval
F0(lamval arg, lamval *env);
extern
lamval
F1(lamval arg, lamval *env);
extern
lamval
F2(lamval arg, lamval *env);

extern
lamval
F0_funclo();
extern
lamval
F1_funclo(lamval n);
extern
lamval
F2_funclo(lamval n, lamval loop, lamval i);

/* ****** ****** */

lamval
F0_funclo()
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo;
  p0->fenv = malloc(1*sizeof(void*));
  p0->fenv[0] = &F0; return (lamval)p0;
}
lamval
F1_funclo(lamval n)
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo;
  p0->fenv = malloc(2*sizeof(void*));
  p0->fenv[0] = &F1; p0->fenv[1] = n; return (lamval)p0;
}
lamval
F2_funclo
(lamval n, lamval loop, lamval i)
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo;
  p0->fenv = malloc(4*sizeof(void*));
  p0->fenv[0] = &F2; p0->fenv[1] = n; p0->fenv[2] = loop; p0->fenv[3] = i; return (lamval)p0;
}

/* ****** ****** */

lamval
F0
(lamval n, lamval *env)
{
  return FUNCLO_APP(FUNCLO_APP(F1_funclo(n), T3Vint(0)), T3Vint(1));
}
lamval
F1
(lamval i, lamval *env)
{
  return F2_funclo(env[1], F1_funclo(env[1]), i);
}
lamval
F2
(lamval r, lamval *env)
{
  T3Iift
  (T3Vopr_lt(env[3], env[1]))
  {
    return
    FUNCLO_APP(FUNCLO_APP(env[2], T3Vopr_add(env[3], T3Vint(1))), T3Vopr_mul(T3Vopr_add(env[3], T3Vint(1)), r));
  } else {
    return r;
  }
}

/* ****** ****** */

int
main()
{
  lamval Rf, Rr;
  Rf = F0_funclo();
  Rr = FUNCLO_APP(Rf, T3Vint(10));
  printf("fact(10) = %i\n", ((lamval_int)Rr)->data);
  return 0;
}

/* ****** ****** */

/* end of [T1Mfact1_lam.c] */
