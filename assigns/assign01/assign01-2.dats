(* ****** ****** *)
//
// Title:
// Principles of
// Programming Languages
// Course: CAS CS 525
//
// Semester: Fall, 2018
//
// Classroom: FLR 112
// Class Time: TR 2:00-3:15
//
// Instructor:
// Hongwei Xi (hwxiATcsDOTbuDOTedu)
//
(* ****** ****** *)
//
// Due date: Tuesday, the 25th of September
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

typedef tvar = string // variable
typedef topr = string // operator

(* ****** ****** *)

datatype term =
//
  | TMint of int // value
  | TMbool of bool // value
  | TMstring of string // value
//
  | TMvar of tvar // not evaluated
  | TMlam of (tvar, term) // value
  | TMapp of (term, term) // non-value
//
  | TMopr of (topr, termlst)
  | TMlet of (tvar, term, term) // let x = t1 in t2 end
//

where termlst = list0(term)
  
(* ****** ****** *)
//
// HX-2018-09-17: 20 points
//
// Please implement substitution
// Note subst(t0,x,t) means to replace
// every free occurrence of [x] with [t]
//
extern
fun
subst : (term, tvar, term) -> term
//
(* ****** ****** *)

(* end of [assign01-2.dats] *)
