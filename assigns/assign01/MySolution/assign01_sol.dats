(* ****** ****** *)
//
// How to test
// ./assign01_sol_dats
//
// How to compile:
// myatscc assign01_sol.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign01.dats"

(* ****** ****** *)
//
// Please implement your code as follows:
//
(* ****** ****** *)

(* end of [assign01_sol.dats] *)
