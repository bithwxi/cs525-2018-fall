(* ****** ****** *)
//
// How to test
// ./assign01-2_sol_dats
//
// How to compile:
// myatscc assign01-2_sol.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign01-2.dats"

(* ****** ****** *)
//
// Please implement your code as follows:
//
(* ****** ****** *)

implement
subst(t0, x, t) =
(
case+ t0 of
//
| TMint _ => t0
| TMbool _ => t0
| TMstring _ => t0
//
(*
| TMvar y => ...
*)
//
(*
| TMlam(y, t1) => ...
*)
//
| TMapp(t1, t2) =>
  TMapp(subst(t1, x, t), subst(t2, x, t))
//
(*
| TMlet
  (x, t1, t2) => ...
*)
(*
| TMopr(opr, ts) => ...
*)
//
)

(* ****** ****** *)

(* end of [assign01-2_sol.dats] *)
