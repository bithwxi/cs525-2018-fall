(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

abstype txyz = ptr

(* ****** ****** *)
//
extern
fun
txyz_new((*void*)): txyz
//
(* ****** ****** *)

extern
fun
eq_txyz_txyz(txyz, txyz): bool

overload = with eq_txyz_txyz

(* ****** ****** *)

extern
fun
print_txyz : (txyz) -> void
and
prerr_txyz : (txyz) -> void
extern
fun
fprint_txyz : (FILEref, txyz) -> void

overload print with print_txyz
overload prerr with prerr_txyz
overload fprint with fprint_txyz

(* ****** ****** *)
//
datatype type =
  | TPvar of txyz
  | TPbas of string
  | TPfun of (type, type)
  | TPtup of (type, type)
//
(* ****** ****** *)

typedef typelst = list0(type)
typedef typeopt = option0(type)

(* ****** ****** *)

datatype
ctype = CTP of (typelst, type)

(* ****** ****** *)

macdef TPint = TPbas("int")
macdef TPbool = TPbas("bool")
macdef TPstring = TPbas("string")

(* ****** ****** *)

typedef tvar = string
typedef topr = string

(* ****** ****** *)
//
datatype term =
//
  | TMint of int // value
  | TMbool of bool // value
(*
  | TMfloat of bool // value
*)
  | TMstring of string // value
//
  | TMvar of tvar // not evaluated
  | TMlam of (tvar, typeopt, term) // value
  | TMapp of (term, term) // non-value
//
  | TMlet of (tvar, term, term) // let x = t1 in t2
  | TMopr of (topr, list0(term)) // primitive operators
//
  | TMift of (term, term, term)
  | TMfix of
    (tvar(*f*), tvar(*x*), typeopt(*arg*), typeopt(*res*), term) // fixed-point opr
//
  | TMtup of (term, term)
  | TMfst of (term) | TMsnd of (term)
//
  | TMann of (term, type) // type annotation
//  
(* ****** ****** *)
//
typedef termlst = list0(term)
//
(* ****** ****** *)

extern
fun
print_type : (type) -> void
and
prerr_type : (type) -> void
extern
fun
fprint_type : (FILEref, type) -> void

overload print with print_type
overload prerr with prerr_type
overload fprint with fprint_type

(* ****** ****** *)

extern
fun
print_term : (term) -> void
and
prerr_term : (term) -> void
extern
fun
fprint_term : (FILEref, term) -> void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_type(t0) = 
fprint_type(stdout_ref, t0)
implement
prerr_type(t0) = 
fprint_type(stderr_ref, t0)

local

implement
fprint_val<type> = fprint_type

in (* in-of-local *)

implement
fprint_type
  (out, T0) =
(
case+ T0 of
//
| TPvar(X) =>
  fprint!(out, "TPvar(", X, ")")
| TPbas(name) =>
  fprint!(out, "TPbas(", name, ")")
| TPfun(arg, res) =>
  fprint!(out, "TPfun(", arg, "; ", res, ")")
| TPtup(fst, snd) =>
  fprint!(out, "TPtup(", fst, "; ", snd, ")")
//
) (* end of [fprint_type] *)

end // end of [local]

(* ****** ****** *)

implement
print_term(t0) = 
fprint_term(stdout_ref, t0)
implement
prerr_term(t0) = 
fprint_term(stderr_ref, t0)

local

implement
fprint_val<term> = fprint_term

in (* in-of-local *)

implement
fprint_term
  (out, t0) =
(
case+ t0 of
//
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMbool(x) =>
  fprint!(out, "TMbool(", x, ")")
| TMstring(x) =>
  fprint!(out, "TMstring(", x, ")")
//
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
//
| TMlam(x1, T1, t2) =>
  fprint!(out, "TMlam(", x1, "; ", t2, ")")
| TMapp(t1, t2) =>
  fprint!(out, "TMapp(", t1, "; ", t2, ")")
//
| TMopr(opr, ts) =>
  fprint!(out, "TMopr(", opr, "; ", ts)
//
| TMlet(x, t1, t2) =>
  fprint!(out, "TMlet(", x, "; ", t1, "; ", t2, ")")
//
| TMift(t1, t2, t3) =>
  fprint!(out, "TMift(", t1, "; ", t2, "; ", t3, ")")
| TMfix(f1, x2, T1, T2, t3) =>
  fprint!(out, "TMfix(", f1, "; ", x2, "; ", t3, ")")
//
| TMtup(t1, t2) =>
  fprint!(out, "TMtup(", t1, "; ", t2, ")")
//
| TMfst(t1) =>
  fprint!(out, "TMfst(", t1, ")")
| TMsnd(t1) =>
  fprint!(out, "TMsnd(", t1, ")")
//
| TMann(t1, T2) =>
  fprint!(out, "TMann(", t1, "; ", T2, ")")
//
)

end // end of [local]

(* ****** ****** *)

(* end of [syntax.dats] *)
