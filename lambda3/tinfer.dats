(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#define :: list0_cons

(* ****** ****** *)

#staload "./syntax.dats"

(* ****** ****** *)

local

val Sigma =
g0ofg1
(
$list{$tup(topr, ctype)}
(
$tup("+", CTP(TPint :: TPint :: nil0(), TPint))
,
$tup("-", CTP(TPint :: TPint :: nil0(), TPint))
,
$tup("*", CTP(TPint :: TPint :: nil0(), TPint))
,
$tup("=", CTP(TPint :: TPint :: nil0(), TPbool))
,
$tup("!=", CTP(TPint :: TPint :: nil0(), TPbool))
,
$tup("<", CTP(TPint :: TPint :: nil0(), TPbool))
,
$tup(">", CTP(TPint :: TPint :: nil0(), TPbool))
,
$tup("<=", CTP(TPint :: TPint :: nil0(), TPbool))
,
$tup(">=", CTP(TPint :: TPint :: nil0(), TPbool))
)
)
typedef Sigma_t = list0($tup(topr, ctype))

in
//
extern
fun
tfind_opr: (topr) -> ctype
//
implement
tfind_opr(opr) =
loop(Sigma) where
{
fun
loop(kxs: Sigma_t): ctype =
(
case- kxs of
| kx :: kxs => if kx.0 = opr then kx.1 else loop(kxs)
)
}
//
end // end of [local]

(* ****** ****** *)

extern
fun
tpunopt: (typeopt) -> type
//
implement
tpunopt(T0) =
(
case+ T0 of
| None0() => TPvar(txyz_new()) | Some0(T) => T
)

(* ****** ****** *)
//
extern
fun
tcsolve: (type, type) -> void
and
tcsolvelst: (typelst, typelst) -> void
//
(* ****** ****** *)

extern
fun
occursck: (txyz, type) -> bool

(* ****** ****** *)

typedef
tcntx = list0($tup(tvar, type))

extern
fun
tinfer0(t0: term): type
extern
fun
tinfer_cntx(t0: term, G0: tcntx): type

(* ****** ****** *)

implement
tinfer_cntx
  (t0, G0) = let
//
(*
val () =
println! ("tinfer_cntx: t0 = ", t0)
val () =
println! ("tinfer_cntx: G0 = ", G0)
*)
//
in
(
case+ t0 of
//
| TMint _ => TPint
| TMbool _ => TPbool
| TMstring _ => TPstring
//
| TMvar(x0) =>
  loop(G0) where
  {
    fun loop(xts: tcntx): type =
    (
      case- xts of
(*
      | nil0() => print...
*)
      | xt :: xts =>
        if x0 = xt.0 then xt.1 else loop(xts)
    )
  }
//
| TMtup(t1, t2) =>
  TPtup(T1, T2) where
  {
    val T1 = tinfer_cntx(t1, G0)
    val T2 = tinfer_cntx(t2, G0)
  }
//
| TMlam(x1, T1, t2) =>
  TPfun(T1, T2) where
  {
    val T1 = tpunopt(T1)
    val G1 = $tup(x1, T1) :: G0
    val T2 = tinfer_cntx(t2, G1)
  }
//
| TMapp(t1, t2) => T12 where
  {
    val T1 = tinfer_cntx(t1, G0)
    val T2 = tinfer_cntx(t2, G0) // T1 = T2 -> T12
    val X2 = txyz_new()
    val T12 = TPvar(X2)
    val Tf = TPfun(T2, T12)
    val () = tcsolve(T1, Tf)
  }
//
| TMfix
  (f1, x2, T1, T2, t3) => let
     val T1 = tpunopt(T1)
     val T2 = tpunopt(T2)
     val Tf = TPfun(T1, T2)
     val G1 = $tup(x2, T1) :: G0
     val G2 = $tup(f1, Tf) :: G1
     val T3 = tinfer_cntx(t3, G2)
     val () = tcsolve(T2, T3)
   in
     Tf
   end
//
| TMift(t1, t2, t3) => let
    val T1 = tinfer_cntx(t1, G0)
    val () = tcsolve(T1, TPbool)
    val T2 = tinfer_cntx(t2, G0)
    val T3 = tinfer_cntx(t3, G0)
    val () = tcsolve(T2, T3)
  in
    T2
  end
//
| TMopr(opr, ts) => T0 where
  {
    val CTP(TS1, T0) = tfind_opr(opr)
    val TS2 =
    list0_map<term><type>(ts, lam(t) => tinfer_cntx(t, G0))
    val () = tcsolvelst(TS1, TS2)
  }
//
| TMlet(x, t1, t2) => let
    val T1 =
      tinfer_cntx(t1, G0)
    val G1 = $tup(x, T1) :: G0
  in
    tinfer_cntx(t2, G1)
  end
//
| TMfst(t12) => let
    val T12 = tinfer_cntx(t12, G0)
    val X1 = txyz_new()
    val X2 = txyz_new()
    val T1 = TPvar(X1)
    val T2 = TPvar(X2)
    val () = tcsolve(T12, TPtup(T1, T2))
  in
    T1
  end
//
| TMsnd(t12) => let
    val T12 = tinfer_cntx(t12, G0)
    val X1 = txyz_new()
    val X2 = txyz_new()
    val T1 = TPvar(X1)
    val T2 = TPvar(X2)
    val () = tcsolve(T12, TPtup(T1, T2))
  in
    T2
  end
//
| TMann(t1, T2) => let
    val T1 = tinfer_cntx(t1, G0)
    val () = tcsolve(T1, T2)
  in
    T2
  end
//
)
end // end of [tinfer_cntx]

(* ****** ****** *)

(* end of [tinfer.dats] *)
